package com.egyoctosoft.zwalltablet.rest;


import com.egyoctosoft.zwalltablet.StartActivity;
import com.egyoctosoft.zwalltablet.models.Categories;
import com.egyoctosoft.zwalltablet.models.Checkin;
import com.egyoctosoft.zwalltablet.models.CreateOrder;
import com.egyoctosoft.zwalltablet.models.Login;
import com.egyoctosoft.zwalltablet.models.Menu;
import com.egyoctosoft.zwalltablet.models.OpenProfile;
import com.egyoctosoft.zwalltablet.models.OrderDetail;
import com.egyoctosoft.zwalltablet.models.Register;
import com.egyoctosoft.zwalltablet.models.Response;
import com.egyoctosoft.zwalltablet.models.Subcategories;
import com.egyoctosoft.zwalltablet.models.Wallet;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;


/**
 * Created by vezikon on 1/28/15.
 */
public interface Api {

    @POST("/MenuAndOrders2/Service1.svc/buildmenu")
    void mainMenu(@Body StartActivity.BranchData gson, Callback<Menu> callback);

    @POST("/MenuAndOrders2/Service1.svc/getCategories")
    void categories(@Body StartActivity.BranchData gson, Callback<Categories> callback);

    @POST("/MenuAndOrders2/Service1.svc/getMenuSubCategories")
    void subCategories(@Body StartActivity.BranchData gson, Callback<Subcategories> callback);

    @POST("/ProfileService2/Service1.svc/signInm")
    void login(@Body Login login, Callback<Response> callback);

    @POST("/ProfileService2/Service1.svc/createProfile")
    void register(@Body Register register, Callback<Response> callback);

    @POST("/ProfileService2/Service1.svc/openProfile")
    void openProfile(@Body OpenProfile openProfile, Callback<Response> callback);

    @POST("/ProfileService2/Service1.svc/checkIn")
    void checkIn(@Body Checkin checkin, Callback<Response> callback);

    @POST("/pointManagement2/Service1.svc/getWallet")
    void wallet(@Body Wallet wallet, Callback<Wallet> callback);

    @POST("/MenuAndOrders2/Service1.svc/CreateOrder")
    void createOrder(@Body CreateOrder order, Callback<Response> callback);

    @POST("/MenuAndOrders2/Service1.svc/AddOrderDetail")
    void addOrderDetail(@Body OrderDetail orderDetail,  Callback<retrofit.client.Response> callback);

}
