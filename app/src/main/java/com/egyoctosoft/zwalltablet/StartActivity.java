package com.egyoctosoft.zwalltablet;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import com.egyoctosoft.zwalltablet.data.MenuDbHelper;
import com.egyoctosoft.zwalltablet.managers.ZwallApp;
import com.egyoctosoft.zwalltablet.models.Categories;
import com.egyoctosoft.zwalltablet.models.ItemCategory;
import com.egyoctosoft.zwalltablet.models.ItemMenu;
import com.egyoctosoft.zwalltablet.models.ItemScreen;
import com.egyoctosoft.zwalltablet.models.ItemSubCategory;
import com.egyoctosoft.zwalltablet.models.Menu;
import com.egyoctosoft.zwalltablet.models.Subcategories;
import com.egyoctosoft.zwalltablet.rest.RestClient;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.egyoctosoft.zwalltablet.data.MenuContract.*;


public class StartActivity extends AppCompatActivity {

    ArrayList<ItemSubCategory> subCategoriesList;
    ArrayList<ItemMenu> menusList;
    ArrayList<ItemCategory> categoriesList;

    ArrayList<ItemScreen> screenItemFilter = new ArrayList<>();

    @InjectView(R.id.login_progress)
    ProgressBar loginProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.inject(this);


        loadCategories();


    }

    private void loadCategories() {
        RestClient.get().categories(new BranchData(), new Callback<Categories>() {
            @Override
            public void success(Categories categories, Response response) {
                categoriesList = (ArrayList<ItemCategory>) categories.getGetCategoriesResult();
                loadSubCategories();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void loadSubCategories() {

        RestClient.get().subCategories(new BranchData(), new Callback<Subcategories>() {
            @Override
            public void success(Subcategories categories, Response response) {
                subCategoriesList = (ArrayList<ItemSubCategory>) categories.getGetMenuSubCategoriesResult();

                if (subCategoriesList != null) {
                    loadMenu();

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
            }
        });
    }

    private void loadMenu() {

        RestClient.get().mainMenu(new BranchData(), new Callback<Menu>() {
            @Override
            public void success(Menu menu, Response response) {
                menusList = (ArrayList<ItemMenu>) menu.getBuildMenuResult();
                if (menusList != null)
                    filterMenu();
                Log.e("Menu size", String.valueOf(menu.getBuildMenuResult().size()));

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
            }
        });
    }

    private void filterMenu() {
        HashMap<String, ArrayList<ItemMenu>> menuHashMap = new HashMap<>();
        ArrayList<ItemMenu> filteredMenu;

        for (ItemSubCategory subCategory : subCategoriesList) {
            filteredMenu = new ArrayList<>();

            for (ItemMenu itemMenu : menusList) {
                if (subCategory.getID() == itemMenu.getMenuItemType())
                    filteredMenu.add(itemMenu);
            }

            menuHashMap.put(subCategory.getMenuItemTypeName(), filteredMenu);

        }

        ZwallApp app = ZwallApp.getInstance();
        app.setMenuMap(menuHashMap);
        app.setCategoriesList(categoriesList);
        app.setSubCategoryList(subCategoriesList);


        getContentResolver().delete(CategoryEntry.CONTENT_URI, null, null);
        getContentResolver().delete(SubCategoryEntry.CONTENT_URI, null, null);
        getContentResolver().delete(MenuEntry.CONTENT_URI, null, null);
        getContentResolver().delete(ScreenItemEntry.CONTENT_URI, null, null);
        getContentResolver().delete(ScreenItemsModifiersBridgeEntry.CONTENT_URI, null, null);

        for (ItemCategory category : categoriesList) {
            getContentResolver().insert(CategoryEntry.CONTENT_URI, insertCategory(category));

        }

        for (ItemSubCategory subCategory : subCategoriesList) {
            getContentResolver().insert(SubCategoryEntry.CONTENT_URI, insertSubCategory(subCategory, subCategory.getCategoryID()));
        }

        for (ItemMenu menu : menusList) {

            getContentResolver().insert(MenuEntry.CONTENT_URI, insertMenuItem(menu, menu.getMenuItemType()));
//            if (!menu.getItemScreens().isEmpty()) {
//                for (ItemScreen itemScreen : menu.getItemScreens()) {
//                    db.insertScreenItem(itemScreen);
//                }
//            }
            //dirty work
            if (!menu.getItemScreens().isEmpty()) {

                for (ItemScreen itemScreen : menu.getItemScreens()) {
                    getContentResolver().insert(ScreenItemEntry.CONTENT_URI, insertScreenItem(itemScreen));

                }
            }

        }


        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }


    public class BranchData {
        String branchID = "5e378f55-a08e-4475-9708-d9639cc3842d";
        String branch = "5e378f55-a08e-4475-9708-d9639cc3842d";
        String venueId ="10" ;

        public String getBranchID() {
            return branchID;
        }

        public String getBranch() {
            return branch;
        }

        public String getVenueId() {
            return venueId;
        }
    }


    public ContentValues insertCategory(ItemCategory category) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CategoryEntry._ID, category.getID());
        contentValues.put(CategoryEntry.COLUMN_CATEGORY_INDEX, category.getCategoryIndex());
        contentValues.put(CategoryEntry.COLUMN_CATEGORY_NAME, category.getCategoryName());
        contentValues.put(CategoryEntry.COLUMN_CATEGORY_NAME_AR, category.getCategoryNameAR());
        contentValues.put(CategoryEntry.COLUMN_IS_ACTIVE, category.getIsActive());
        contentValues.put(CategoryEntry.COLUMN_IS_SCREEN_ONE_VISIBLE, category.getIsScreenOneVisible());
        return contentValues;
    }

    public ContentValues insertSubCategory(ItemSubCategory subCategory, int categoryID) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SubCategoryEntry._ID, subCategory.getID());
        contentValues.put(SubCategoryEntry.COLUMN_IS_ACTIVE, subCategory.getIsActive());
        contentValues.put(SubCategoryEntry.COLUMN_IS_SCREEN_ONE_VISIBLE, subCategory.getIsScreenOneVisible());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_CUSINIE, subCategory.getMenuItemTypeCuisine());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_DESC, subCategory.getMenuItemTypeDesc());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_DESC_AR, subCategory.getMenuItemTypeDescAR());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME, subCategory.getMenuItemTypeName());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME_AR, subCategory.getMenuItemTypeNameAR());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_IMAGE, subCategory.getMenuItemTypeImage());
        contentValues.put(SubCategoryEntry.COLUMN_TYPE_INDEX, subCategory.getTypeIndex());
        contentValues.put(SubCategoryEntry.COLUMN_CAT_KEY, subCategory.getCategoryID());

        return contentValues;

    }

    public ContentValues insertScreenItem(ItemScreen screen) {

        ContentValues contentValues = null;
        contentValues = new ContentValues();
        contentValues.put(ScreenItemEntry._ID, screen.getID());
        contentValues.put(ScreenItemEntry.COLUMN_MAX_CHOICES, screen.getMaxChoices());
        contentValues.put(ScreenItemEntry.COLUMN_MAX_FREE, screen.getMaxFree());
        contentValues.put(ScreenItemEntry.COLUMN_MIN_CHOICES, screen.getMinChoices());
        contentValues.put(ScreenItemEntry.COLUMN_PARENT_ITEM, screen.getParentItem());
        contentValues.put(ScreenItemEntry.COLUMN_SCREEN_INDEX, screen.getScreenIndex());
        contentValues.put(ScreenItemEntry.COLUMN_SCREEN_NAME, screen.getScreenName());
        contentValues.put(ScreenItemEntry.COLUMN_SCREEN_NAME_AR, screen.getScreenNameAR());
        contentValues.put(ScreenItemEntry.COLUMN_MENU_KEY, screen.getParentItem());


        ContentValues bridgeContentValues = null;
        for (Integer i : screen.getScreenItmes()) {
            bridgeContentValues = new ContentValues();
            bridgeContentValues.put(ScreenItemsModifiersBridgeEntry.COLUMN_MENU_ITEM_ID, i);
            bridgeContentValues.put(ScreenItemsModifiersBridgeEntry.COLUMN_SCREEN_ITEM_ID, screen.getID());

            getContentResolver().insert(ScreenItemsModifiersBridgeEntry.CONTENT_URI, bridgeContentValues);
        }


        return contentValues;
    }

    public ContentValues insertMenuItem(ItemMenu menu, int subcategoryID) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MenuEntry._ID, menu.getID());

        contentValues.put(MenuEntry.COLUMN_SUB_CATEGORY_KEY, subcategoryID);
        contentValues.put(MenuEntry.COLUMN_ACTIVE, menu.isAcitve());
        contentValues.put(MenuEntry.COLUMN_ALLOW_SPECIAL_INSTRUCTIONS, menu.isAllowSpecialInstructions());
        contentValues.put(MenuEntry.COLUMN_BRANCH_ID, menu.getBranchID());
        contentValues.put(MenuEntry.COLUMN_ESCAPE_MODE, menu.getEscapeMode());
        contentValues.put(MenuEntry.COLUMN_HAS_ADJECTIVES, menu.isHasAdjectives() ? 0 : 1);
        contentValues.put(MenuEntry.COLUMN_IMAGE, menu.getImage());
        contentValues.put(MenuEntry.COLUMN_IS_SCREEN_ONE_VISIBLE, menu.getIsScreenOneVisible());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_CUISINE, menu.getMenuItemCusine());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION, menu.getMenuItemDescription());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION_AR, menu.getMenuItemDescriptionAR());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_NAME, menu.getMenuItemName());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_NAME_AR, menu.getMenuItemNameAR());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_POINTS, menu.getMenuItemPoints());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_PRICE, menu.getMenuItemPrice());
        contentValues.put(MenuEntry.COLUMN_MODIFIER, menu.getModifier());
        contentValues.put(MenuEntry.COLUMN_SERVICE_CHARGE, menu.isServiceCharge());
        contentValues.put(MenuEntry.COLUMN_SERVICE_CHARGE_PERCENTAGE, menu.getServiceChargePercentage());
        contentValues.put(MenuEntry.COLUMN_TAX_PERCENTAGE, menu.getTaxPercentage());
        contentValues.put(MenuEntry.COLUMN_TAXABLE, menu.isTaxable());
        contentValues.put(MenuEntry.COLUMN_NO_SCREEN_ITEMS, menu.getItemScreens().isEmpty() ? 0 : 1);

        return contentValues;
    }

    private void screenItemsFilter(ItemScreen itemScreen) {

        boolean isNotInserted = true;

        for (ItemScreen screen : screenItemFilter) {
            if (screen.getID() == itemScreen.getID()) {
                isNotInserted = false;
            }
        }

        if (isNotInserted)
            screenItemFilter.add(itemScreen);
    }
}
