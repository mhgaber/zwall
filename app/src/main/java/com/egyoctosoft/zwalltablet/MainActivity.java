package com.egyoctosoft.zwalltablet;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.egyoctosoft.zwalltablet.fragments.CategoryDialogFragment;
import com.egyoctosoft.zwalltablet.fragments.MyOrderDialog;
import com.egyoctosoft.zwalltablet.managers.ZwallApp;
import com.egyoctosoft.zwalltablet.models.ItemOrder;
import com.egyoctosoft.zwalltablet.models.ItemSubCategory;
import com.egyoctosoft.zwalltablet.models.OpenProfile;
import com.egyoctosoft.zwalltablet.views.adapters.CustomersBarAdapter;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.rey.material.widget.ImageButton;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.egyoctosoft.zwalltablet.data.MenuContract.CategoryEntry;
import static com.egyoctosoft.zwalltablet.data.MenuContract.SubCategoryEntry;
import static com.egyoctosoft.zwalltablet.fragments.CategoryDialogFragment.OnCategoryDialogListener;
import static com.egyoctosoft.zwalltablet.fragments.MainMenuFragment.*;
import static com.egyoctosoft.zwalltablet.views.adapters.CustomersBarAdapter.*;


public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>,
        OnCategoryDialogListener, OnCustomerSelectionListener, OnMenuListener {

    public static final int RESULT_NEW_CUSTOMER = 0;
    public static final String KEY_ADD_NEW_CUSTOMER = "new.customer";
    public static final String MENU_ITEM = "menu.item";

    private static final String TAG = MainActivity.class.getName();

    //UI
    @InjectView(R.id.materialViewPager)
    MaterialViewPager mViewPager;
    private Toolbar mToolbar;
    private TextView title;
    private ImageButton categoryBtn;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private TextView orderQtyTv;
    private LinearLayout myOrderLayout;


    //app manager (singleton)
    ZwallApp app;


    //specify the columns that we need from stored data
    private static final String[] CATEGORIES_COLUMNS = {
            CategoryEntry.TABLE_NAME + "." + CategoryEntry._ID,
            CategoryEntry.COLUMN_CATEGORY_NAME,
            SubCategoryEntry.TABLE_NAME + "." + SubCategoryEntry._ID,
            SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME,
            SubCategoryEntry.COLUMN_CAT_KEY,
            SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_IMAGE

    };

    //these indices are tied to CATEGORIES_COLUMNS.
    // if CATEGORIES_COLUMNS changes, it must changes
    private static final int COL_CAT_ID = 0;
    private static final int COL_CAT_NAME = 1;
    private static final int COL_SUBCAT_ID = 2;
    private static final int COL_SUBCAT_NAME = 3;
    private static final int COL_CAT_KEY = 4;
    private static final int COL_SUBCAT_IMG = 5;


    //customer list
    private ArrayList<Object> customersList = new ArrayList<>();
    //Subcategories
    private ArrayList<ItemSubCategory> itemSubCategoryArrayList;

    //customer toolbar
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    //cart hash map
    HashMap<String, ArrayList<ItemOrder>> cartMap = new HashMap<>();
    OpenProfile currentUser;

    int qty = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbarr_title);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        categoryBtn = (ImageButton) findViewById(R.id.menu_btn_btn);
        categoryBtn.setOnClickListener(this);

        orderQtyTv = (TextView) findViewById(R.id.order_qty);
        orderQtyTv.setText(String.valueOf(qty));

        myOrderLayout = (LinearLayout) findViewById(R.id.my_order_btn);
        myOrderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMyOrders();
            }
        });

        //setting mToolbar as a part of the view pager
        mViewPager.setToolbar(mToolbar);

        if (mToolbar != null) {
            setSupportActionBar(mViewPager.getToolbar());

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayUseLogoEnabled(false);
                actionBar.setHomeButtonEnabled(true);
            }
        }


        app = ZwallApp.getInstance();

        customersList.addAll(app.getCustomers());
        customersList.add(getString(R.string.new_guest));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        //setting the drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, 0, 0);
        mDrawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        getSupportLoaderManager().initLoader(0, null, this);

        //setting up the customers toolbar
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);


        // specify an adapter (see also next example)
        mAdapter = new CustomersBarAdapter(customersList, this, this);
//        TestAdapter adapter = new TestAdapter(customersList);
        mRecyclerView.setAdapter(mAdapter);


    }

    /**
     * Use this method to set the View pager and tabs' titles
     */
    private void setViewPager() {

        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            int oldPosition = -3;

            @Override
            public Fragment getItem(int position) {
                return newInstance(itemSubCategoryArrayList.get(position).getID());
            }

            @Override
            public int getCount() {
                return itemSubCategoryArrayList.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return itemSubCategoryArrayList.get(position).getMenuItemTypeName();
            }

            //called when the current page has changed
            @Override
            public void setPrimaryItem(ViewGroup container, int position, Object object) {
                super.setPrimaryItem(container, position, object);

                //only if position changed
                if (position == oldPosition)
                    return;
                oldPosition = position;

                //subcategory image
                String imageUrl = itemSubCategoryArrayList.get(position).getMenuItemTypeImage();
                int color = getResources().getColor(R.color.black);

                final int fadeDuration = 400;

                //change header's color and image
                mViewPager.setImageUrl(imageUrl, fadeDuration);
                mViewPager.setColor(color, fadeDuration);

            }

        });

        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        MaterialViewPagerHelper.registerViewPager(this, mViewPager.getPagerTitleStrip());

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.menu_btn_btn:
                showCategoryDialog();
                break;
        }


    }

    /**
     * use this method to show Categories dialog.
     */
    private void showCategoryDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        DialogFragment dialogFragment = CategoryDialogFragment.newInstance();
        dialogFragment.show(ft, "categoriesDialog");
    }

    /**
     * use this method to show my orders dialog.
     */
    private void showMyOrders() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        DialogFragment dialogFragment = MyOrderDialog.newInstance(cartMap);
        dialogFragment.show(ft, "myordersdialog");
    }

    /*
     * Loader manager
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                CategoryEntry.buildCategoryWithSubcategoryUri()
                , CATEGORIES_COLUMNS,
                null,
                null,
                CategoryEntry.COLUMN_CATEGORY_INDEX);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {


        if (data.getCount() > 0) {
            data.moveToFirst();

            title.setText(data.getString(COL_CAT_NAME));

            itemSubCategoryArrayList = new ArrayList<>();

            do {

                ItemSubCategory itemSubCategory = new ItemSubCategory();
                itemSubCategory.setID(data.getInt(COL_SUBCAT_ID));
                itemSubCategory.setMenuItemTypeName(data.getString(COL_SUBCAT_NAME));
                itemSubCategory.setMenuItemTypeImage(data.getString(COL_SUBCAT_IMG));
                itemSubCategory.setCategoryID(data.getInt(COL_CAT_KEY));
                itemSubCategoryArrayList.add(itemSubCategory);

            } while (data.moveToNext());

            //setting the pager
            setViewPager();
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /*
     * Category dialog listener
     */
    @Override
    public void onCategoryChanged(int id, String name) {

        title.setText(name);

        for (int i = 0; i < itemSubCategoryArrayList.size(); i++) {
            if (itemSubCategoryArrayList.get(i).getCategoryID() == id) {
                mViewPager.getViewPager().setCurrentItem(i);
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_NEW_CUSTOMER:
//                customersList.clear();
//                customersList.addAll(app.getCustomers());
//                customersList.add("new guest");
//                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    /*
     * Customer bar listeners
     */
    @Override
    public void onSelected(OpenProfile openProfile, int position) {

        for (Object object : customersList) {
            if (object instanceof OpenProfile) {

                OpenProfile op = (OpenProfile) object;

                if (openProfile.getID() != op.getID()) {
                    op.setIsSelected(false);
                } else {
                    op.setIsSelected(true);
                }

            }
        }

        currentUser = openProfile;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewCustomer() {

        //start login routine and get back when you get some results
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        MainActivity.this.startActivityForResult(intent, RESULT_NEW_CUSTOMER);
    }

    /*
     * main menu fragment listeners
     */
    @Override
    public void onAddItem(ItemOrder itemOrder) {

        if (currentUser == null) {
            Toast.makeText(MainActivity.this, "Please, Choose user ", Toast.LENGTH_SHORT).show();
            return;
        }
        String userID = currentUser.getID();

        if (cartMap.containsKey(userID)) {
            cartMap.get(userID).add(itemOrder);
        } else {
            ArrayList<ItemOrder> itemOrders = new ArrayList<>();
            itemOrders.add(itemOrder);

            cartMap.put(userID, itemOrders);
        }

        orderQtyTv.setText("" + ++qty);
        Log.i(TAG, "" + qty);
    }
}
