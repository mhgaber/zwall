package com.egyoctosoft.zwalltablet.models;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by vezikon on 7/14/15.
 */
public class Register {


    private String firstname;
    private String lastname;
    private String mphoneno;
    private String nickname;
    private String email;
    private String dateOfBirth;
    private boolean genderIsMale;
    private String password;
    private String pictureURL;
    private String coverPic;
    private int status = 0;
    private String currentBranchId;
    private String tableID;
    private int userTypeId;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMphoneno() {
        return mphoneno;
    }

    public void setMphoneno(String mphoneno) {
        this.mphoneno = mphoneno;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isGenderIsMale() {
        return genderIsMale;
    }

    public void setGenderIsMale(boolean genderIsMale) {
        this.genderIsMale = genderIsMale;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        md.update(password.getBytes());

        byte byteData[] = md.digest();

        // convert the byte to hex format method 1
        StringBuffer sbs = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sbs.append(Integer.toString((byteData[i] & 0xff) + 0x100,
                    16).substring(1));
        }
        this.password = sbs.toString();
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getCoverPic() {
        return coverPic;
    }

    public void setCoverPic(String coverPic) {
        this.coverPic = coverPic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCurrentBranchId() {
        return currentBranchId;
    }

    public void setCurrentBranchId(String currentBranchId) {
        this.currentBranchId = currentBranchId;
    }

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }
}
