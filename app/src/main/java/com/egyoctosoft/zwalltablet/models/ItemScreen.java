package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class ItemScreen {

    int ID;
    int MaxChoices;
    int MaxFree;
    int ParentItem;
    int ScreenIndex;
    String ScreenName;
    String ScreenNameAR;
    int minChoices;
    List<Integer> screenItmes = new ArrayList<Integer>();

    /**
     * @return The ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID The ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * @return The MaxChoices
     */
    public int getMaxChoices() {
        return MaxChoices;
    }

    /**
     * @param MaxChoices The MaxChoices
     */
    public void setMaxChoices(int MaxChoices) {
        this.MaxChoices = MaxChoices;
    }

    /**
     * @return The MaxFree
     */
    public int getMaxFree() {
        return MaxFree;
    }

    /**
     * @param MaxFree The MaxFree
     */
    public void setMaxFree(int MaxFree) {
        this.MaxFree = MaxFree;
    }

    /**
     * @return The ParentItem
     */
    public int getParentItem() {
        return ParentItem;
    }

    /**
     * @param ParentItem The ParentItem
     */
    public void setParentItem(int ParentItem) {
        this.ParentItem = ParentItem;
    }

    /**
     * @return The ScreenIndex
     */
    public int getScreenIndex() {
        return ScreenIndex;
    }

    /**
     * @param ScreenIndex The ScreenIndex
     */
    public void setScreenIndex(int ScreenIndex) {
        this.ScreenIndex = ScreenIndex;
    }

    /**
     * @return The ScreenName
     */
    public String getScreenName() {
        return ScreenName;
    }

    /**
     * @param ScreenName The ScreenName
     */
    public void setScreenName(String ScreenName) {
        this.ScreenName = ScreenName;
    }

    /**
     * @return The ScreenNameAR
     */
    public String getScreenNameAR() {
        return ScreenNameAR;
    }

    /**
     * @param ScreenNameAR The ScreenNameAR
     */
    public void setScreenNameAR(String ScreenNameAR) {
        this.ScreenNameAR = ScreenNameAR;
    }

    /**
     * @return The minChoices
     */
    public int getMinChoices() {
        return minChoices;
    }

    /**
     * @param minChoices The minChoices
     */
    public void setMinChoices(int minChoices) {
        this.minChoices = minChoices;
    }

    /**
     * @return The screenItmes
     */
    public List<Integer> getScreenItmes() {
        return screenItmes;
    }

    /**
     * @param screenItmes The screenItmes
     */
    public void setScreenItmes(List<Integer> screenItmes) {
        this.screenItmes = screenItmes;
    }


}
