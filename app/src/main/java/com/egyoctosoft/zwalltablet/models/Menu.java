package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class Menu {
     List<ItemMenu> buildMenuResult = new ArrayList<ItemMenu>();

    /**
     * @return The buildMenuResult
     */
    public List<ItemMenu> getBuildMenuResult() {
        return buildMenuResult;
    }

    /**
     * @param buildMenuResult The buildMenuResult
     */
    public void setBuildMenuResult(List<ItemMenu> buildMenuResult) {
        this.buildMenuResult = buildMenuResult;
    }


}
