package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class ItemCategory {

    Integer CategoryIndex;
    String CategoryName;
    String CategoryNameAR;
    Integer ID;
    Boolean IsActive;
    Boolean IsScreenOneVisible;

    /**
     * @return The CategoryIndex
     */
    public Integer getCategoryIndex() {
        return CategoryIndex;
    }

    /**
     * @param CategoryIndex The CategoryIndex
     */
    public void setCategoryIndex(Integer CategoryIndex) {
        this.CategoryIndex = CategoryIndex;
    }

    /**
     * @return The CategoryName
     */
    public String getCategoryName() {
        return CategoryName;
    }

    /**
     * @param CategoryName The CategoryName
     */
    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    /**
     * @return The CategoryNameAR
     */
    public String getCategoryNameAR() {
        return CategoryNameAR;
    }

    /**
     * @param CategoryNameAR The CategoryNameAR
     */
    public void setCategoryNameAR(String CategoryNameAR) {
        this.CategoryNameAR = CategoryNameAR;
    }

    /**
     * @return The ID
     */
    public Integer getID() {
        return ID;
    }

    /**
     * @param ID The ID
     */
    public void setID(Integer ID) {
        this.ID = ID;
    }

    /**
     * @return The IsActive
     */
    public Boolean getIsActive() {
        return IsActive;
    }

    /**
     * @param IsActive The IsActive
     */
    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    /**
     * @return The IsScreenOneVisible
     */
    public Boolean getIsScreenOneVisible() {
        return IsScreenOneVisible;
    }

    /**
     * @param IsScreenOneVisible The IsScreenOneVisible
     */
    public void setIsScreenOneVisible(Boolean IsScreenOneVisible) {
        this.IsScreenOneVisible = IsScreenOneVisible;
    }

}
