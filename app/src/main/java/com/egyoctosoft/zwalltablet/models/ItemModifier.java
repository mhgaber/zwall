package com.egyoctosoft.zwalltablet.models;

import android.graphics.Color;

import org.parceler.Parcel;

/**
 * Created by vezikon on 8/4/15.
 */
@Parcel
public class ItemModifier extends ItemMenu {

    int count = 0;
    int color = Color.TRANSPARENT;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
