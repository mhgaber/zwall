package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class ItemSubCategory {

    Integer CategoryID;
    Integer ID;
    Boolean IsActive;
    Boolean IsScreenOneVisible;
    Integer MenuItemTypeCuisine;
    String MenuItemTypeDesc;
    String MenuItemTypeDescAR;
    String MenuItemTypeImage;
    String MenuItemTypeName;
    String MenuItemTypeNameAR;
    Integer TypeIndex;

    /**
     * @return The CategoryID
     */
    public Integer getCategoryID() {
        return CategoryID;
    }

    /**
     * @param CategoryID The CategoryID
     */
    public void setCategoryID(Integer CategoryID) {
        this.CategoryID = CategoryID;
    }

    /**
     * @return The ID
     */
    public Integer getID() {
        return ID;
    }

    /**
     * @param ID The ID
     */
    public void setID(Integer ID) {
        this.ID = ID;
    }

    /**
     * @return The IsActive
     */
    public Boolean getIsActive() {
        return IsActive;
    }

    /**
     * @param IsActive The IsActive
     */
    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    /**
     * @return The IsScreenOneVisible
     */
    public Boolean getIsScreenOneVisible() {
        return IsScreenOneVisible;
    }

    /**
     * @param IsScreenOneVisible The IsScreenOneVisible
     */
    public void setIsScreenOneVisible(Boolean IsScreenOneVisible) {
        this.IsScreenOneVisible = IsScreenOneVisible;
    }

    /**
     * @return The MenuItemTypeCuisine
     */
    public Integer getMenuItemTypeCuisine() {
        return MenuItemTypeCuisine;
    }

    /**
     * @param MenuItemTypeCuisine The MenuItemTypeCuisine
     */
    public void setMenuItemTypeCuisine(Integer MenuItemTypeCuisine) {
        this.MenuItemTypeCuisine = MenuItemTypeCuisine;
    }

    /**
     * @return The MenuItemTypeDesc
     */
    public String getMenuItemTypeDesc() {
        return MenuItemTypeDesc;
    }

    /**
     * @param MenuItemTypeDesc The MenuItemTypeDesc
     */
    public void setMenuItemTypeDesc(String MenuItemTypeDesc) {
        this.MenuItemTypeDesc = MenuItemTypeDesc;
    }

    /**
     * @return The MenuItemTypeDescAR
     */
    public String getMenuItemTypeDescAR() {
        return MenuItemTypeDescAR;
    }

    /**
     * @param MenuItemTypeDescAR The MenuItemTypeDescAR
     */
    public void setMenuItemTypeDescAR(String MenuItemTypeDescAR) {
        this.MenuItemTypeDescAR = MenuItemTypeDescAR;
    }

    /**
     * @return The MenuItemTypeImage
     */
    public String getMenuItemTypeImage() {
        return MenuItemTypeImage;
    }

    /**
     * @param MenuItemTypeImage The MenuItemTypeImage
     */
    public void setMenuItemTypeImage(String MenuItemTypeImage) {
        this.MenuItemTypeImage = MenuItemTypeImage;
    }

    /**
     * @return The MenuItemTypeName
     */
    public String getMenuItemTypeName() {
        return MenuItemTypeName;
    }

    /**
     * @param MenuItemTypeName The MenuItemTypeName
     */
    public void setMenuItemTypeName(String MenuItemTypeName) {
        this.MenuItemTypeName = MenuItemTypeName;
    }

    /**
     * @return The MenuItemTypeNameAR
     */
    public String getMenuItemTypeNameAR() {
        return MenuItemTypeNameAR;
    }

    /**
     * @param MenuItemTypeNameAR The MenuItemTypeNameAR
     */
    public void setMenuItemTypeNameAR(String MenuItemTypeNameAR) {
        this.MenuItemTypeNameAR = MenuItemTypeNameAR;
    }

    /**
     * @return The TypeIndex
     */
    public Integer getTypeIndex() {
        return TypeIndex;
    }

    /**
     * @param TypeIndex The TypeIndex
     */
    public void setTypeIndex(Integer TypeIndex) {
        this.TypeIndex = TypeIndex;
    }
}
