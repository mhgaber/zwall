package com.egyoctosoft.zwalltablet.models;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by vezikon on 7/14/15.
 */
public class Checkin {

    private String userID;
    private String branchID;
    private String password;
    private int tableId;
    private int venueID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getPassword() throws NoSuchAlgorithmException {
        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public int getVenueID() {
        return venueID;
    }

    public void setVenueID(int venueID) {
        this.venueID = venueID;
    }
}
