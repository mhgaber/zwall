package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class ItemMenu {

    boolean Acitve;
    boolean AllowSpecialInstructions;
    String BranchID;
    int EscapeMode;
    boolean HasAdjectives;
    int ID;
    int IsScreenOneVisible;
    List<ItemScreen> ItemScreens = new ArrayList<ItemScreen>();
    int MenuItemCusine;
    String MenuItemDescription;
    String MenuItemDescriptionAR;
    String MenuItemName;
    String MenuItemNameAR;
    int MenuItemPoints;
    float MenuItemPrice;
    int MenuItemRPoints;
    int MenuItemType;
    int Price10;
    int Price11;
    int Price12;
    int Price13;
    int Price14;
    int Price2;
    int Price3;
    int Price4;
    int Price5;
    int Price6;
    int Price7;
    int Price8;
    int Price9;
    boolean ServiceCharge;
    int ServiceChargePercentage;
    int TaxPercentage;
    boolean Taxable;
    int VenueID;
    String image;
    int modifier;
    int posCode;
    boolean hasScreenItems;

    /**
     * @return The Acitve
     */
    public boolean isAcitve() {
        return Acitve;
    }

    /**
     * @param Acitve The Acitve
     */
    public void setAcitve(boolean Acitve) {
        this.Acitve = Acitve;
    }

    /**
     * @return The AllowSpecialInstructions
     */
    public boolean isAllowSpecialInstructions() {
        return AllowSpecialInstructions;
    }

    /**
     * @param AllowSpecialInstructions The AllowSpecialInstructions
     */
    public void setAllowSpecialInstructions(boolean AllowSpecialInstructions) {
        this.AllowSpecialInstructions = AllowSpecialInstructions;
    }

    /**
     * @return The BranchID
     */
    public String getBranchID() {
        return BranchID;
    }

    /**
     * @param BranchID The BranchID
     */
    public void setBranchID(String BranchID) {
        this.BranchID = BranchID;
    }

    /**
     * @return The EscapeMode
     */
    public int getEscapeMode() {
        return EscapeMode;
    }

    /**
     * @param EscapeMode The EscapeMode
     */
    public void setEscapeMode(int EscapeMode) {
        this.EscapeMode = EscapeMode;
    }

    /**
     * @return The HasAdjectives
     */
    public boolean isHasAdjectives() {
        return HasAdjectives;
    }

    /**
     * @param HasAdjectives The HasAdjectives
     */
    public void setHasAdjectives(boolean HasAdjectives) {
        this.HasAdjectives = HasAdjectives;
    }

    /**
     * @return The ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID The ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * @return The IsScreenOneVisible
     */
    public int getIsScreenOneVisible() {
        return IsScreenOneVisible;
    }

    /**
     * @param IsScreenOneVisible The IsScreenOneVisible
     */
    public void setIsScreenOneVisible(int IsScreenOneVisible) {
        this.IsScreenOneVisible = IsScreenOneVisible;
    }

    /**
     * @return The ItemScreens
     */
    public List<ItemScreen> getItemScreens() {
        return ItemScreens;
    }

    /**
     * @param ItemScreens The ItemScreens
     */
    public void setItemScreens(List<ItemScreen> ItemScreens) {
        this.ItemScreens = ItemScreens;
    }

    /**
     * @return The MenuItemCusine
     */
    public int getMenuItemCusine() {
        return MenuItemCusine;
    }

    /**
     * @param MenuItemCusine The MenuItemCusine
     */
    public void setMenuItemCusine(int MenuItemCusine) {
        this.MenuItemCusine = MenuItemCusine;
    }

    /**
     * @return The MenuItemDescription
     */
    public String getMenuItemDescription() {
        return MenuItemDescription;
    }

    /**
     * @param MenuItemDescription The MenuItemDescription
     */
    public void setMenuItemDescription(String MenuItemDescription) {
        this.MenuItemDescription = MenuItemDescription;
    }

    /**
     * @return The MenuItemDescriptionAR
     */
    public String getMenuItemDescriptionAR() {
        return MenuItemDescriptionAR;
    }

    /**
     * @param MenuItemDescriptionAR The MenuItemDescriptionAR
     */
    public void setMenuItemDescriptionAR(String MenuItemDescriptionAR) {
        this.MenuItemDescriptionAR = MenuItemDescriptionAR;
    }

    /**
     * @return The MenuItemName
     */
    public String getMenuItemName() {
        return MenuItemName;
    }

    /**
     * @param MenuItemName The MenuItemName
     */
    public void setMenuItemName(String MenuItemName) {
        this.MenuItemName = MenuItemName;
    }

    /**
     * @return The MenuItemNameAR
     */
    public String getMenuItemNameAR() {
        return MenuItemNameAR;
    }

    /**
     * @param MenuItemNameAR The MenuItemNameAR
     */
    public void setMenuItemNameAR(String MenuItemNameAR) {
        this.MenuItemNameAR = MenuItemNameAR;
    }

    /**
     * @return The MenuItemPoints
     */
    public int getMenuItemPoints() {
        return MenuItemPoints;
    }

    /**
     * @param MenuItemPoints The MenuItemPoints
     */
    public void setMenuItemPoints(int MenuItemPoints) {
        this.MenuItemPoints = MenuItemPoints;
    }

    /**
     * @return The MenuItemPrice
     */
    public float getMenuItemPrice() {
        return MenuItemPrice;
    }

    /**
     * @param MenuItemPrice The MenuItemPrice
     */
    public void setMenuItemPrice(float MenuItemPrice) {
        this.MenuItemPrice = MenuItemPrice;
    }

    /**
     * @return The MenuItemRPoints
     */
    public int getMenuItemRPoints() {
        return MenuItemRPoints;
    }

    /**
     * @param MenuItemRPoints The MenuItemRPoints
     */
    public void setMenuItemRPoints(int MenuItemRPoints) {
        this.MenuItemRPoints = MenuItemRPoints;
    }

    /**
     * @return The MenuItemType
     */
    public int getMenuItemType() {
        return MenuItemType;
    }

    /**
     * @param MenuItemType The MenuItemType
     */
    public void setMenuItemType(int MenuItemType) {
        this.MenuItemType = MenuItemType;
    }

    /**
     * @return The Price10
     */
    public int getPrice10() {
        return Price10;
    }

    /**
     * @param Price10 The Price10
     */
    public void setPrice10(int Price10) {
        this.Price10 = Price10;
    }

    /**
     * @return The Price11
     */
    public int getPrice11() {
        return Price11;
    }

    /**
     * @param Price11 The Price11
     */
    public void setPrice11(int Price11) {
        this.Price11 = Price11;
    }

    /**
     * @return The Price12
     */
    public int getPrice12() {
        return Price12;
    }

    /**
     * @param Price12 The Price12
     */
    public void setPrice12(int Price12) {
        this.Price12 = Price12;
    }

    /**
     * @return The Price13
     */
    public int getPrice13() {
        return Price13;
    }

    /**
     * @param Price13 The Price13
     */
    public void setPrice13(int Price13) {
        this.Price13 = Price13;
    }

    /**
     * @return The Price14
     */
    public int getPrice14() {
        return Price14;
    }

    /**
     * @param Price14 The Price14
     */
    public void setPrice14(int Price14) {
        this.Price14 = Price14;
    }

    /**
     * @return The Price2
     */
    public int getPrice2() {
        return Price2;
    }

    /**
     * @param Price2 The Price2
     */
    public void setPrice2(int Price2) {
        this.Price2 = Price2;
    }

    /**
     * @return The Price3
     */
    public int getPrice3() {
        return Price3;
    }

    /**
     * @param Price3 The Price3
     */
    public void setPrice3(int Price3) {
        this.Price3 = Price3;
    }

    /**
     * @return The Price4
     */
    public int getPrice4() {
        return Price4;
    }

    /**
     * @param Price4 The Price4
     */
    public void setPrice4(int Price4) {
        this.Price4 = Price4;
    }

    /**
     * @return The Price5
     */
    public int getPrice5() {
        return Price5;
    }

    /**
     * @param Price5 The Price5
     */
    public void setPrice5(int Price5) {
        this.Price5 = Price5;
    }

    /**
     * @return The Price6
     */
    public int getPrice6() {
        return Price6;
    }

    /**
     * @param Price6 The Price6
     */
    public void setPrice6(int Price6) {
        this.Price6 = Price6;
    }

    /**
     * @return The Price7
     */
    public int getPrice7() {
        return Price7;
    }

    /**
     * @param Price7 The Price7
     */
    public void setPrice7(int Price7) {
        this.Price7 = Price7;
    }

    /**
     * @return The Price8
     */
    public int getPrice8() {
        return Price8;
    }

    /**
     * @param Price8 The Price8
     */
    public void setPrice8(int Price8) {
        this.Price8 = Price8;
    }

    /**
     * @return The Price9
     */
    public int getPrice9() {
        return Price9;
    }

    /**
     * @param Price9 The Price9
     */
    public void setPrice9(int Price9) {
        this.Price9 = Price9;
    }

    /**
     * @return The ServiceCharge
     */
    public boolean isServiceCharge() {
        return ServiceCharge;
    }

    /**
     * @param ServiceCharge The ServiceCharge
     */
    public void setServiceCharge(boolean ServiceCharge) {
        this.ServiceCharge = ServiceCharge;
    }

    /**
     * @return The ServiceChargePercentage
     */
    public int getServiceChargePercentage() {
        return ServiceChargePercentage;
    }

    /**
     * @param ServiceChargePercentage The ServiceChargePercentage
     */
    public void setServiceChargePercentage(int ServiceChargePercentage) {
        this.ServiceChargePercentage = ServiceChargePercentage;
    }

    /**
     * @return The TaxPercentage
     */
    public int getTaxPercentage() {
        return TaxPercentage;
    }

    /**
     * @param TaxPercentage The TaxPercentage
     */
    public void setTaxPercentage(int TaxPercentage) {
        this.TaxPercentage = TaxPercentage;
    }

    /**
     * @return The Taxable
     */
    public boolean isTaxable() {
        return Taxable;
    }

    /**
     * @param Taxable The Taxable
     */
    public void setTaxable(boolean Taxable) {
        this.Taxable = Taxable;
    }

    /**
     * @return The VenueID
     */
    public int getVenueID() {
        return VenueID;
    }

    /**
     * @param VenueID The VenueID
     */
    public void setVenueID(int VenueID) {
        this.VenueID = VenueID;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The modifier
     */
    public int getModifier() {
        return modifier;
    }

    /**
     * @param modifier The modifier
     */
    public void setModifier(int modifier) {
        this.modifier = modifier;
    }

    /**
     * @return The posCode
     */
    public int getPosCode() {
        return posCode;
    }

    /**
     * @param posCode The posCode
     */
    public void setPosCode(int posCode) {
        this.posCode = posCode;
    }

    public boolean hasScreenItems() {
        return hasScreenItems;
    }

    public void setHasScreenItems(boolean hasScreenItems) {
        this.hasScreenItems = hasScreenItems;
    }
}
