package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by vezikon on 8/10/15.
 */
@Parcel
public class ItemOrder extends ItemMenu {

    ArrayList<ItemModifier> sides = new ArrayList<>();

    public ArrayList<ItemModifier> getSides() {
        return sides;
    }

    public void setSides(ArrayList<ItemModifier> sides) {
        this.sides = sides;
    }


}
