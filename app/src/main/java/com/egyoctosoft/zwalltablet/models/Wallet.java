package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by vezikon on 7/15/15.
 */
public class Wallet {


    private ArrayList<ItemWallet> getWalletResult = new ArrayList<>();
    private String userID;

    public ArrayList<ItemWallet> getWalletResult() {
        return getWalletResult;
    }

    public void setWalletResult(ArrayList<ItemWallet> getWalletResult) {
        this.getWalletResult = getWalletResult;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String target) {
        this.userID = target;
    }

    @Parcel
    public static class ItemWallet {

        int credit;
        int debit;
        String VenueName;
        String VenueNameAR;
        String UserID;
        int VenueID;

        public int getCredit() {
            return credit;
        }

        public void setCredit(int credit) {
            this.credit = credit;
        }

        public int getDebit() {
            return debit;
        }

        public void setDebit(int debit) {
            this.debit = debit;
        }

        public String getVenueName() {
            return VenueName;
        }

        public void setVenueName(String venueName) {
            VenueName = venueName;
        }

        public String getVenueNameAR() {
            return VenueNameAR;
        }

        public void setVenueNameAR(String venueNameAR) {
            VenueNameAR = venueNameAR;
        }

        public String getUserID() {
            return UserID;
        }

        public void setUserID(String userID) {
            UserID = userID;
        }

        public int getVenueID() {
            return VenueID;
        }

        public void setVenueID(int venueID) {
            VenueID = venueID;
        }
    }
}
