package com.egyoctosoft.zwalltablet.models;

import java.util.ArrayList;

/**
 * Created by vezikon on 8/10/15.
 */
public class OrderDetail {

    // {"branchID":"819584d6-8a4c-404a-bc01-7922b475a18f","userId":"8de6f607-510b-4057-b0d9-2999cbb3f2ce","specialinstructions":"[\"\",\"\",\"\"]",
    // "payInpoints":"[false,false,false]","isfreeModefire":"[true,true,true]","discountId":1,
    // "password":"ec278a38901287b2771a13739520384d43e4b078f78affe702def108774cce24","menuItems":"[44,46,46]","orderId":"674594b8-5e33-4bc5-abbb-19f7041e97b2"}

    ArrayList<Integer> menuItems = new ArrayList<>();
    ArrayList<Boolean> payInpoints = new ArrayList<>();
    ArrayList<Boolean> isfreeModefire = new ArrayList<>();
    ArrayList<String> specialinstructions = new ArrayList<>();

    int discountId = 1;
    String orderId;
    String userId;
    String password;
    String branchID;

    public ArrayList<Integer> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(ArrayList<Integer> menuItems) {
        this.menuItems = menuItems;
    }

    public ArrayList<Boolean> getPayInpoints() {
        return payInpoints;
    }

    public void setPayInpoints(ArrayList<Boolean> payInpoints) {
        this.payInpoints = payInpoints;
    }

    public ArrayList<Boolean> getIsfreeModefire() {
        return isfreeModefire;
    }

    public void setIsfreeModefire(ArrayList<Boolean> isfreeModefire) {
        this.isfreeModefire = isfreeModefire;
    }

    public ArrayList<String> getSpecialinstructions() {
        return specialinstructions;
    }

    public void setSpecialinstructions(ArrayList<String> specialinstructions) {
        this.specialinstructions = specialinstructions;
    }

    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }
}
