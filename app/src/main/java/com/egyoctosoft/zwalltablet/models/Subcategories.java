package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class Subcategories {
     List<ItemSubCategory> getMenuSubCategoriesResult = new ArrayList<ItemSubCategory>();

    /**
     * @return The getMenuSubCategoriesResult
     */
    public List<ItemSubCategory> getGetMenuSubCategoriesResult() {
        return getMenuSubCategoriesResult;
    }

    /**
     * @param getMenuSubCategoriesResult The getMenuSubCategoriesResult
     */
    public void setGetMenuSubCategoriesResult(List<ItemSubCategory> getMenuSubCategoriesResult) {
        this.getMenuSubCategoriesResult = getMenuSubCategoriesResult;
    }
}
