package com.egyoctosoft.zwalltablet.models;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by vezikon on 7/14/15.
 */
public class Login {

    String mobile;
    String password;

    public String getPhoneNumber() {
        return mobile;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mobile = phoneNumber;
    }

    public String getPassword() throws NoSuchAlgorithmException {
       return password;
    }

    public void setEncryptedPassword(String password){
        this.password = password;
    }
    public void setPassword(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        md.update(password.getBytes());

        byte byteData[] = md.digest();

        // convert the byte to hex format method 1
        StringBuffer sbs = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sbs.append(Integer.toString((byteData[i] & 0xff) + 0x100,
                    16).substring(1));
        }
        this.password = sbs.toString();
    }

}
