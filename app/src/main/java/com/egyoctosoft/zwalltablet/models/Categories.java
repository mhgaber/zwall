package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vezikon on 5/18/15.
 */
@Parcel
public class Categories {

    private List<ItemCategory> getCategoriesResult = new ArrayList<ItemCategory>();

    /**
     *
     * @return
     * The getCategoriesResult
     */
    public List<ItemCategory> getGetCategoriesResult() {
        return getCategoriesResult;
    }

    /**
     *
     * @param getCategoriesResult
     * The getCategoriesResult
     */
    public void setGetCategoriesResult(List<ItemCategory> getCategoriesResult) {
        this.getCategoriesResult = getCategoriesResult;
    }
}
