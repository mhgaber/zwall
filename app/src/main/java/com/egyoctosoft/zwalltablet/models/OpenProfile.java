package com.egyoctosoft.zwalltablet.models;

import org.parceler.Parcel;

/**
 * Created by vezikon on 7/14/15.
 */
@Parcel
public class OpenProfile {


    int userType;
    boolean IsActive;
    boolean SuspendByAdmin;
    boolean SuspendByOperator;
    boolean SuspendByUser;
    String CurrentBranchId;
    String ID;
    String FirstName;
    String LastName;
    String Password;
    String target;
    boolean isSelected = false;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setIsActive(boolean isActive) {
        IsActive = isActive;
    }

    public boolean isSuspendByAdmin() {
        return SuspendByAdmin;
    }

    public void setSuspendByAdmin(boolean suspendByAdmin) {
        SuspendByAdmin = suspendByAdmin;
    }

    public boolean isSuspendByOperator() {
        return SuspendByOperator;
    }

    public void setSuspendByOperator(boolean suspendByOperator) {
        SuspendByOperator = suspendByOperator;
    }

    public boolean isSuspendByUser() {
        return SuspendByUser;
    }

    public void setSuspendByUser(boolean suspendByUser) {
        SuspendByUser = suspendByUser;
    }

    public String getCurrentBranchId() {
        return CurrentBranchId;
    }

    public void setCurrentBranchId(String currentBranchId) {
        CurrentBranchId = currentBranchId;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
