package com.egyoctosoft.zwalltablet.models;

/**
 * Created by vezikon on 7/14/15.
 */
public class Response {

    //success
    private String createProfileResult;
    // is not 00000000-0000-0000-0000-000000000000
    private String signInMResult;
    // done
    private String checkInResult;

    private OpenProfile openProfileResult;

    private CreateOrderResult CreateOrderResult;

    public String getCreateProfileResult() {
        return createProfileResult;
    }

    public void setCreateProfileResult(String createProfileResult) {
        this.createProfileResult = createProfileResult;
    }

    public String getSignInMResult() {
        return signInMResult;
    }

    public void setSignInMResult(String signInMResult) {
        this.signInMResult = signInMResult;
    }

    public String getCheckInResult() {
        return checkInResult;
    }

    public void setCheckInResult(String checkInResult) {
        this.checkInResult = checkInResult;
    }

    public OpenProfile getOpenProfileResult() {
        return openProfileResult;
    }

    public void setOpenProfileResult(OpenProfile openProfileResult) {
        this.openProfileResult = openProfileResult;
    }

    public Response.CreateOrderResult getCreateOrderResult() {
        return CreateOrderResult;
    }
    

    public class CreateOrderResult{
       // {"CreateOrderResult":{"BranchID":"5e378f55-a08e-4475-9708-d9639cc3842d",
       // "Delivered":false,
       // "ID":"6d4f4087-357f-4c2d-9fd4-5421f040ddc5",
       // "OrderRef":"",
       // "OrderType":0,
       // "PickUpTime":0,
       // "TotalCash":0.00,"TotalPoints":0,
       // "UserID":"b8b71524-407e-4c47-b7b8-e4a4f9642e8b",
       // "VenueID":0,
       // "isOpen":true,
       // "timeStamp":"\/Date(1439236071923+0200)\/"}}

        String ID;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }
    }

    // {"AddOrderDetailResult":
    // [{"ConfirmedByUserTimeSend":"\/Date(-6847812000000+0200)\/","DiscountID":1,"EventTriggerId":0,"FreeModifier":false,"ID":14241,"KitchanTimeSend":"\/Date(-6847812000000+0200)\/","MenuItemID":542,"OrderID":"90c06c73-0eed-46df-96f9-1abc46269ef9","OrderTimeSend":"\/Date(1439238093877+0200)\/","PayInPoints":false,"ServedTimeSend":"\/Date(-6847812000000+0200)\/","Specialinstructions":" ","WaiterId":"00000000-0000-0000-0000-000000000000","casherId":"00000000-0000-0000-0000-000000000000"},
    // {"ConfirmedByUserTimeSend":"\/Date(-6847812000000+0200)\/","DiscountID":1,"EventTriggerId":0,"FreeModifier":false,"ID":14242,"KitchanTimeSend":"\/Date(-6847812000000+0200)\/","MenuItemID":542,"OrderID":"90c06c73-0eed-46df-96f9-1abc46269ef9","OrderTimeSend":"\/Date(1439238093890+0200)\/","PayInPoints":false,"ServedTimeSend":"\/Date(-6847812000000+0200)\/","Specialinstructions":" ","WaiterId":"00000000-0000-0000-0000-000000000000","casherId":"00000000-0000-0000-0000-000000000000"},
    // {"ConfirmedByUserTimeSend":"\/Date(-6847812000000+0200)\/","DiscountID":1,"EventTriggerId":0,"FreeModifier":false,"ID":14243,"KitchanTimeSend":"\/Date(-6847812000000+0200)\/","MenuItemID":542,"OrderID":"90c06c73-0eed-46df-96f9-1abc46269ef9","OrderTimeSend":"\/Date(1439238093890+0200)\/","PayInPoints":false,"ServedTimeSend":"\/Date(-6847812000000+0200)\/","Specialinstructions":" ","WaiterId":"00000000-0000-0000-0000-000000000000","casherId":"00000000-0000-0000-0000-000000000000"}]}

}
