package com.egyoctosoft.zwalltablet.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.models.ItemModifier;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by vezikon on 8/4/15.
 */
public class ModifiersAdapter extends BaseAdapter {

    ArrayList<ItemModifier> itemModifiersList;
    Context context;
    OnModifiersAdapterClickListener listener;
    int screenItemId;

    public ModifiersAdapter(ArrayList<ItemModifier> itemModifiersList, Context context,
                            OnModifiersAdapterClickListener listener, int screenItemId) {
        this.itemModifiersList = itemModifiersList;
        this.context = context;
        this.screenItemId = screenItemId;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return itemModifiersList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemModifiersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_modifier, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ItemModifier itemModifier = (ItemModifier) getItem(position);

        holder.modifierName.setText(itemModifier.getMenuItemName());
        holder.modifierPrice.setText(String.valueOf(itemModifier.getMenuItemPrice()));
        holder.modifierQuantity.setText(String.valueOf(itemModifier.getCount()));

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onAddClicked(itemModifier, position, screenItemId);
            }
        });

        holder.subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onSubtractClicked(itemModifier, position, screenItemId);
            }
        });


        return convertView;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_modifier.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.modifier_name)
        TextView modifierName;
        @InjectView(R.id.modifier_price)
        TextView modifierPrice;
        @InjectView(R.id.modifier_quantity)
        TextView modifierQuantity;
        @InjectView(R.id.item_modifier_background)
        LinearLayout background;
        @InjectView(R.id.modifier_add)
        Button add;
        @InjectView(R.id.modifier_sub)
        Button subtract;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnModifiersAdapterClickListener {
        public void onAddClicked(ItemModifier modifier, int position, int screenItemId);

        public void onSubtractClicked(ItemModifier modifier, int position, int screenItemId);

    }
}
