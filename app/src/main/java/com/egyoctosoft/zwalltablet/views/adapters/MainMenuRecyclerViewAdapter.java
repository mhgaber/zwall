package com.egyoctosoft.zwalltablet.views.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.models.ItemMenu;
import com.squareup.picasso.Picasso;
import com.rey.material.widget.Button;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by vezikon on 5/21/15.
 */
public class MainMenuRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<ItemMenu> menu;
    Context context;
    int width;
    int height;
    OnMenuItemClickListener listener;


    public MainMenuRecyclerViewAdapter(ArrayList<ItemMenu> menu, Context context
            , OnMenuItemClickListener listener) {
        this.menu = menu;
        this.context = context;
        this.listener = listener;

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.placeholder);
        width = largeIcon.getWidth();
        height = largeIcon.getHeight();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_menu, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;

        final ItemMenu item = menu.get(position);

        vh.name.setText(item.getMenuItemName().trim());
        vh.desc.setText(item.getMenuItemDescription().trim());

        Picasso.with(vh.img.getContext()).cancelRequest(vh.img);
        Picasso.with(context)
                .load(item.getImage())
                .placeholder(R.drawable.placeholder)
                .centerInside().fit()
                .into(vh.img);

        vh.itemView.setTag(item);

        //HTML formatted text for the different text colors, Thanks selim :/
        String text = "<font color=#000000 size=\"6\"><b>"
                + item.getMenuItemPrice()
                + "</font> <font color=#9fbc1d size=\"2\"> LE</font>"
                + "<br><font color=#fd7400 size=\"5\"><b>ORDER</font>";

        vh.order.setText(Html.fromHtml(text));
        vh.order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (item.hasScreenItems() || item.isHasAdjectives()) {
//                    Toast.makeText(context, "has modifiers", Toast.LENGTH_SHORT).show();
                    listener.onMenuItemHasModifiers(item);
                } else {
//                    Toast.makeText(context, "has no modifiers", Toast.LENGTH_SHORT).show();
                    listener.onMenuItemOrder(item);
                }
            }
        });

        vh.itemDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemDetails(item);
            }
        });

    }


    @Override
    public int getItemCount() {
        return menu.size();
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_grid_menu.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.item_grid_menu_img)
        ImageView img;
        @InjectView(R.id.item_grid_name)
        TextView name;
        @InjectView(R.id.item_grid_item_details)
        Button itemDetails;
        @InjectView(R.id.item_grid_order)
        Button order;
        @InjectView(R.id.item_grid_desc)
        TextView desc;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMenuItemClickListener {
        void onMenuItemOrder(ItemMenu itemMenu);

        void onItemDetails(ItemMenu itemMenu);

        void onMenuItemHasModifiers(ItemMenu itemMenu);

    }
}
