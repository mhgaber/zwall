package com.egyoctosoft.zwalltablet.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egyoctosoft.zwalltablet.LoginActivity;
import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.models.OpenProfile;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by vezikon on 8/10/15.
 */
public class CustomersBarAdapter extends RecyclerView.Adapter<CustomersBarAdapter.ViewHolder> {
    private ArrayList<Object> mDataset;

    private static final int ITEM_NEW_GUEST = 0;
    private static final int ITEM_CUSTOMER = 1;


    private OnCustomerSelectionListener listener;
    private Context context;

    public CustomersBarAdapter(ArrayList<Object> mDataset, OnCustomerSelectionListener listener,
                               Context context) {
        this.mDataset = mDataset;
        this.listener = listener;
        this.context = context;
    }

    //get the view type
    @Override
    public int getItemViewType(int position) {
        if (mDataset.get(position) instanceof OpenProfile) {
            return ITEM_CUSTOMER;
        } else {
            return ITEM_NEW_GUEST;
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType) {
            case ITEM_NEW_GUEST:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_toolbar_new_customer, parent, false);
                break;
            case ITEM_CUSTOMER:
                // create a new view
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_toolbar_test, parent, false);
                break;
        }

        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ViewHolder vh = holder;

        int type = holder.getItemViewType();


        switch (type) {
            case ITEM_CUSTOMER: {
                final OpenProfile openProfile = (OpenProfile) mDataset.get(position);
                vh.testTv.setText(openProfile.getFirstName() + " " + openProfile.getLastName());

                if (openProfile.isSelected()) {
                    vh.testTv.setBackground(context.getResources().getDrawable(R.drawable.bg_item_selected_customer));
                } else {
                    vh.testTv.setBackground(context.getResources().getDrawable(R.drawable.bg_item_customer));
                }

                vh.testTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onSelected(openProfile, position);
                    }
                });

            }
            break;
            case ITEM_NEW_GUEST: {
                final String data = (String) mDataset.get(position);
                vh.testTv.setText(data);
                vh.testTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        listener.onNewCustomer();
                    }
                });
            }
            break;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_toolbar_test.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        @Optional
        @InjectView(R.id.test_tv)
        TextView testTv;


        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCustomerSelectionListener {
        public void onSelected(OpenProfile openProfile, int position);

        public void onNewCustomer();

    }

}