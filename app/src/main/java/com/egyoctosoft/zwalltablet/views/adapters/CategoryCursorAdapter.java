package com.egyoctosoft.zwalltablet.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.fragments.CategoryDialogFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by vezikon on 7/14/15.
 */
public class CategoryCursorAdapter extends CursorAdapter {


    public CategoryCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_grid_category, null);
        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        viewHolder.itemGridCategory.setText(cursor.getString(CategoryDialogFragment.COL_NAME));
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_grid_category.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.item_grid_category)
        TextView itemGridCategory;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
