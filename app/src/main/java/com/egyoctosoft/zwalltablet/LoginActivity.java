package com.egyoctosoft.zwalltablet;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.egyoctosoft.zwalltablet.fragments.GuestFragment;
import com.egyoctosoft.zwalltablet.fragments.LoginFragment;
import com.egyoctosoft.zwalltablet.fragments.LoginOptionsFragment;
import com.egyoctosoft.zwalltablet.fragments.RegistrationFragment;

import static com.egyoctosoft.zwalltablet.fragments.LoginFragment.*;
import static com.egyoctosoft.zwalltablet.fragments.LoginOptionsFragment.*;
import static com.egyoctosoft.zwalltablet.fragments.RegistrationFragment.*;


public class LoginActivity extends AppCompatActivity implements OnLoginOptionListener,
        OnLoginListener, OnRegistrationListener, GuestFragment.OnGuestListener {

    private boolean addNewCustomer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, new LoginOptionsFragment())
                    .commit();
        }
        if (getIntent() != null)
            getIntent().getBooleanArrayExtra(MainActivity.KEY_ADD_NEW_CUSTOMER);


    }


    @Override
    public void onLoginSuccess() {

        if (addNewCustomer) {
            setResult(RESULT_OK);
            finish();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onOptionClicked(int option) {

        Log.i("option", "" + option);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment;

        switch (option) {
            case LoginOptionsFragment.OPTION_LOGIN:

                fragment = LoginFragment.newInstance();
                ft.replace(R.id.fragment, fragment);
                ft.addToBackStack(null);
                ft.commit();

                break;
            case LoginOptionsFragment.OPTION_GUEST:
                fragment = GuestFragment.newInstance();
                ft.replace(R.id.fragment, fragment);
                ft.addToBackStack(null);
                ft.commit();
                break;
            case LoginOptionsFragment.OPTION_REGISTER:
                fragment = RegistrationFragment.newInstance();
                ft.replace(R.id.fragment, fragment);
                ft.addToBackStack(null);
                ft.commit();
                break;
        }

    }

    @Override
    public void onRegistrationSucess() {
        if (addNewCustomer) {
            setResult(RESULT_OK);
            finish();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onRegister() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment;

        fragment = RegistrationFragment.newInstance();
        ft.replace(R.id.fragment, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onGuestLoginSuccess() {
        if (addNewCustomer) {
            setResult(RESULT_OK);
            finish();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onCancel() {

    }
}
