package com.egyoctosoft.zwalltablet.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;


/**
 * Created by vezikon on 6/14/15.
 */
public class MenuContract {

    public static final String CONTENT_AUTHORITY = "com.egyoctosoft.zwalltablet";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_MENU_ITEMS = "menu_items";
    public static final String PATH_CATEGORIES = "categories";
    public static final String PATH_SUBCATEGORIES = "subcategories";
    public static final String PATH_SCREEN_ITEMS = "screen_items";
    public static final String PATH_SCREEN_ITEMS_MENU_BRIDGE = "menu_bridge";
    public static final String PATH_SCREEN_ITEMS_MODIFIERS_BRIDGE = "modifiers_bridge";


    public static class CategoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_CATEGORIES)
                .build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORIES;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORIES;

        //table name
        public static final String TABLE_NAME = "category";

        //table columns
        public static final String COLUMN_IS_ACTIVE = "isActive";
        public static final String COLUMN_CATEGORY_NAME = "CategoryName";
        public static final String COLUMN_CATEGORY_NAME_AR = "CategoryNameAR";
        public static final String COLUMN_CATEGORY_INDEX = "CategoryIndex";
        public static final String COLUMN_IS_SCREEN_ONE_VISIBLE = "isScreenOneVisible";

        public static Uri buildCategoriesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildCategoryWithSubcategoryWithMenuItemsUri(String catName, String subcatName) {
            return CONTENT_URI.buildUpon().appendPath(catName).appendPath(subcatName).build();
        }

        public static Uri buildCategoryWithSubcategoryUri() {
            return CONTENT_URI.buildUpon().appendPath("test").build();
        }
    }

    public static class SubCategoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_SUBCATEGORIES)
                .build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SUBCATEGORIES;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SUBCATEGORIES;


        //table name
        public static final String TABLE_NAME = "subcategory";

        //table columns
        public static final String COLUMN_IS_ACTIVE = "isActive";
        public static final String COLUMN_IS_SCREEN_ONE_VISIBLE = "isScreenOneVisible";
        public static final String COLUMN_MENU_ITEM_TYPE_CUSINIE = "MenuItemTypeCuisine";
        public static final String COLUMN_MENU_ITEM_TYPE_DESC = "MenuItemTypeDesc";
        public static final String COLUMN_MENU_ITEM_TYPE_DESC_AR = "MenuItemTypeDescAR";
        public static final String COLUMN_MENU_ITEM_TYPE_IMAGE = "MenuItemTypeImage";
        public static final String COLUMN_MENU_ITEM_TYPE_NAME = "MenuItemTypeName";
        public static final String COLUMN_MENU_ITEM_TYPE_NAME_AR = "MenuItemTypeNameAR";
        public static final String COLUMN_TYPE_INDEX = "TypeIndex";

        public static final String COLUMN_CAT_KEY = "category_id";

        public static Uri buildSubcategoryUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildSubCategoryWithMenuItemsUri(String subcatName) {
            return CONTENT_URI.buildUpon().appendPath(subcatName).build();
        }
    }

    public static class MenuEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_MENU_ITEMS)
                .build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MENU_ITEMS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MENU_ITEMS;

        //table name
        public static final String TABLE_NAME = "menu";
        public static final String MODIFIER_TABLE_NAME = "modifier";

        //table columns
        public static final String COLUMN_ACTIVE = "active";
        public static final String COLUMN_ALLOW_SPECIAL_INSTRUCTIONS = "allow_special_instructions";
        public static final String COLUMN_BRANCH_ID = "branch_id";
        public static final String COLUMN_ESCAPE_MODE = "escapeMode";
        public static final String COLUMN_HAS_ADJECTIVES = "hasAdjectives";
        public static final String COLUMN_IS_SCREEN_ONE_VISIBLE = "isScreenOneVisible";
        public static final String COLUMN_MENU_ITEM_CUISINE = "menuItemCuisine";
        public static final String COLUMN_MENU_ITEM_DESCRIPTION = "MenuItemDescription";
        public static final String COLUMN_MENU_ITEM_DESCRIPTION_AR = "MenuItemDescriptionAR";
        public static final String COLUMN_MENU_ITEM_NAME = "MenuItemName";
        public static final String COLUMN_MENU_ITEM_NAME_AR = "MenuItemNameAR";
        public static final String COLUMN_MENU_ITEM_POINTS = "MenuItemPoints";
        public static final String COLUMN_MENU_ITEM_PRICE = "MenuItemPrice";
        public static final String COLUMN_PRICE2 = "price2";
        public static final String COLUMN_PRICE3 = "price3";
        public static final String COLUMN_PRICE4 = "price4";
        public static final String COLUMN_PRICE5 = "price5";
        public static final String COLUMN_PRICE6 = "price6";
        public static final String COLUMN_PRICE7 = "price7";
        public static final String COLUMN_PRICE8 = "price8";
        public static final String COLUMN_PRICE9 = "price9";
        public static final String COLUMN_PRICE10 = "price10";
        public static final String COLUMN_PRICE11 = "price11";
        public static final String COLUMN_PRICE12 = "price12";
        public static final String COLUMN_PRICE13 = "price13";
        public static final String COLUMN_PRICE14 = "price14";
        public static final String COLUMN_SERVICE_CHARGE = "serviceCharge";
        public static final String COLUMN_SERVICE_CHARGE_PERCENTAGE = "serviceChargePercentage";
        public static final String COLUMN_TAX_PERCENTAGE = "taxPercentage";
        public static final String COLUMN_TAXABLE = "taxable";
        public static final String COLUMN_VENUE_ID = "venueID";
        public static final String COLUMN_IMAGE = "image";
        public static final String COLUMN_MODIFIER = "modifier";
        public static final String COLUMN_POS_CODE = "posCode";
        public static final String COLUMN_SUB_CATEGORY_KEY = "subcategory_id";
        public static final String COLUMN_SCREEN_ITEM_KEY = "screen_item_id";
        public static final String COLUMN_NO_SCREEN_ITEMS = "noScreenItems";


        public static Uri buildMenuUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildSubCategoryWithMenuItemsUri(String screenItems) {
            return CONTENT_URI.buildUpon().appendPath(screenItems).build();
        }

        public static Uri buildOneMenuItemUri(int menuItemId) {
            return CONTENT_URI.buildUpon()
                    .appendQueryParameter(_ID, Long.toString(menuItemId)).build();
        }
    }

    public static class ScreenItemEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_SCREEN_ITEMS)
                .build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SCREEN_ITEMS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SCREEN_ITEMS;

        public static final String TABLE_NAME = "screen_item";

        public static final String COLUMN_MAX_CHOICES = "max_choices";
        public static final String COLUMN_MAX_FREE = "max_free";
        public static final String COLUMN_PARENT_ITEM = "parent_item";
        public static final String COLUMN_SCREEN_INDEX = "screen_index";
        public static final String COLUMN_SCREEN_NAME = "screen_name";
        public static final String COLUMN_SCREEN_NAME_AR = "screen_name_ar";
        public static final String COLUMN_MIN_CHOICES = "min_choices";
        public static final String COLUMN_MENU_KEY = "menu_id";
        public static final String COLUMN_BACKEND_ID = "backend_id";

        public static Uri buildScreenItemUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }

//    public static class ScreenItemsMenuBridgeEntry implements BaseColumns {
//        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
//                .appendPath(PATH_SCREEN_ITEMS_MENU_BRIDGE)
//                .build();
//
//        public static final String CONTENT_TYPE =
//                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SCREEN_ITEMS_MENU_BRIDGE;
//        public static final String CONTENT_ITEM_TYPE =
//                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SCREEN_ITEMS_MENU_BRIDGE;
//
//        public static final String TABLE_NAME = "screen_items_menu_bridge";
//
//        public static final String COLUMN_SCREEN_ITEM_ID = "screen_item_id";
//        public static final String COLUMN_MENU_ITEM_ID = "menu_item_id";
//
//        public static Uri buildScreenItemUri(long id) {
//            return ContentUris.withAppendedId(CONTENT_URI, id);
//        }
//    }

    public static class ScreenItemsModifiersBridgeEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_SCREEN_ITEMS_MODIFIERS_BRIDGE)
                .build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SCREEN_ITEMS_MODIFIERS_BRIDGE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SCREEN_ITEMS_MODIFIERS_BRIDGE;

        public static final String TABLE_NAME = "screen_items_modifiers_bridge";

        public static final String COLUMN_SCREEN_ITEM_ID = "screen_item_id";
        public static final String COLUMN_MENU_ITEM_ID = "menu_item_id";

        public static Uri buildScreenItemUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }
}
