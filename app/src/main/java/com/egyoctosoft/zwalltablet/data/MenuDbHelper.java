package com.egyoctosoft.zwalltablet.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.egyoctosoft.zwalltablet.data.MenuContract.CategoryEntry;
import com.egyoctosoft.zwalltablet.data.MenuContract.MenuEntry;
import com.egyoctosoft.zwalltablet.data.MenuContract.SubCategoryEntry;
import com.egyoctosoft.zwalltablet.data.MenuContract.ScreenItemEntry;
import com.egyoctosoft.zwalltablet.models.ItemCategory;
import com.egyoctosoft.zwalltablet.models.ItemMenu;
import com.egyoctosoft.zwalltablet.models.ItemScreen;
import com.egyoctosoft.zwalltablet.models.ItemSubCategory;

import static com.egyoctosoft.zwalltablet.data.MenuContract.*;

/**
 * Created by vezikon on 6/19/15.
 */
public class MenuDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 12;

    static final String DATABASE_NAME = "menu.db";

    public MenuDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_CATEGORY_TABLE = "CREATE TABLE " + CategoryEntry.TABLE_NAME
                + " ("
                + CategoryEntry._ID + " INTEGER PRIMARY KEY, "
                + CategoryEntry.COLUMN_CATEGORY_INDEX + " INTEGER NOT NULL, "
                + CategoryEntry.COLUMN_CATEGORY_NAME + " TEXT NOT NULL, "
                + CategoryEntry.COLUMN_CATEGORY_NAME_AR + " TEXT NOT NULL, "
                + CategoryEntry.COLUMN_IS_ACTIVE + " INTEGER NOT NULL, "
                + CategoryEntry.COLUMN_IS_SCREEN_ONE_VISIBLE + " INTEGER NOT NULL "
                + " );";

        final String SQL_CREATE_SUB_CATEGORY_TABLE = "CREATE TABLE " + SubCategoryEntry.TABLE_NAME
                + " ("
                + SubCategoryEntry._ID + " INTEGER PRIMARY KEY, "
                + SubCategoryEntry.COLUMN_IS_ACTIVE + " INTEGER NOT NULL, "
                + SubCategoryEntry.COLUMN_CAT_KEY + " INTEGER NOT NULL, "
                + SubCategoryEntry.COLUMN_IS_SCREEN_ONE_VISIBLE + " INTEGER NOT NULL, "
                + SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_CUSINIE + " INTEGER NOT NULL, "
                + SubCategoryEntry.COLUMN_TYPE_INDEX + " INTEGER NOT NULL, "
                + SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME + " TEXT NOT NULL, "
                + SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME_AR + " TEXT NOT NULL, "
                + SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_DESC + " TEXT NOT NULL, "
                + SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_DESC_AR + " TEXT NOT NULL, "
                + SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_IMAGE + " TEXT NOT NULL, "
                + " FOREIGN KEY (" + SubCategoryEntry.COLUMN_CAT_KEY + ") REFERENCES " +
                CategoryEntry.TABLE_NAME + " (" + CategoryEntry._ID + ") "
                + " );";

        final String SQL_CREATE_MENU_TABLE = "CREATE TABLE " + MenuEntry.TABLE_NAME
                + " ("
                + MenuEntry._ID + " INTEGER PRIMARY KEY, "
                + MenuEntry.COLUMN_ACTIVE + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_ALLOW_SPECIAL_INSTRUCTIONS + " INTEGER , "
                + MenuEntry.COLUMN_IMAGE + " TEXT NOT NULL, "
                + MenuEntry.COLUMN_BRANCH_ID + " TEXT, "
                + MenuEntry.COLUMN_IS_SCREEN_ONE_VISIBLE + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_NO_SCREEN_ITEMS + " INTEGER DEFAULT 0, "
                + MenuEntry.COLUMN_SUB_CATEGORY_KEY + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_ESCAPE_MODE + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_MENU_ITEM_NAME + " TEXT NOT NULL, "
                + MenuEntry.COLUMN_MENU_ITEM_NAME_AR + " TEXT NOT NULL, "
                + MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION_AR + " TEXT NOT NULL, "
                + MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION + " TEXT NOT NULL, "
                + MenuEntry.COLUMN_HAS_ADJECTIVES + " INTEGER, "
                + MenuEntry.COLUMN_MENU_ITEM_POINTS + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_MENU_ITEM_PRICE + " REAL NOT NULL, "
                + MenuEntry.COLUMN_SERVICE_CHARGE + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_POS_CODE + " INTEGER, "
                + MenuEntry.COLUMN_PRICE2 + " REAL, "
                + MenuEntry.COLUMN_PRICE3 + " REAL, "
                + MenuEntry.COLUMN_PRICE4 + " REAL, "
                + MenuEntry.COLUMN_PRICE5 + " REAL, "
                + MenuEntry.COLUMN_PRICE6 + " REAL, "
                + MenuEntry.COLUMN_PRICE7 + " REAL, "
                + MenuEntry.COLUMN_PRICE8 + " REAL, "
                + MenuEntry.COLUMN_PRICE9 + " REAL, "
                + MenuEntry.COLUMN_PRICE10 + " REAL, "
                + MenuEntry.COLUMN_PRICE11 + " REAL, "
                + MenuEntry.COLUMN_PRICE12 + " REAL, "
                + MenuEntry.COLUMN_PRICE13 + " REAL, "
                + MenuEntry.COLUMN_PRICE14 + " REAL, "
                + MenuEntry.COLUMN_MODIFIER + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_TAX_PERCENTAGE + " REAL NOT NULL, "
                + MenuEntry.COLUMN_SERVICE_CHARGE_PERCENTAGE + " REAL NOT NULL, "
                + MenuEntry.COLUMN_TAXABLE + " INTEGER NOT NULL, "
                + MenuEntry.COLUMN_VENUE_ID + " TEXT, "
                + MenuEntry.COLUMN_MENU_ITEM_CUISINE + " INTEGER, "
                + " FOREIGN KEY (" + MenuEntry.COLUMN_SUB_CATEGORY_KEY + ") REFERENCES " +
                SubCategoryEntry.TABLE_NAME + " (" + SubCategoryEntry._ID + ") "
               /* + " FOREIGN KEY (" + MenuEntry.COLUMN_SCREEN_ITEM_KEY + ") REFERENCES " +
                ScreenItemEntry.TABLE_NAME + " (" + ScreenItemEntry._ID  + ") "*/
                + " );";

        final String SQL_CREATE_SCREEN_ITEM_TABLE = "CREATE TABLE " + ScreenItemEntry.TABLE_NAME
                + " ("
                + ScreenItemEntry._ID + " INTEGER PRIMARY KEY, "
                + ScreenItemEntry.COLUMN_MAX_CHOICES + " INTEGER NOT NULL, "
                + ScreenItemEntry.COLUMN_MAX_FREE + " INTEGER NOT NULL, "
                + ScreenItemEntry.COLUMN_MENU_KEY + " INTEGER NOT NULL, "
                + ScreenItemEntry.COLUMN_MIN_CHOICES + " INTEGER NOT NULL, "
                + ScreenItemEntry.COLUMN_SCREEN_NAME + " TEXT NOT NULL, "
                + ScreenItemEntry.COLUMN_SCREEN_NAME_AR + " TEXT NOT NULL, "
                + ScreenItemEntry.COLUMN_PARENT_ITEM + " INTEGER NOT NULL, "
                + ScreenItemEntry.COLUMN_SCREEN_INDEX + " INTEGER NOT NULL, "
                + " FOREIGN KEY (" + ScreenItemEntry.COLUMN_MENU_KEY + ") REFERENCES " +
                MenuEntry.TABLE_NAME + " (" + MenuEntry._ID + ") " + " ON DELETE CASCADE ON UPDATE CASCADE "
                + " );";

        final String SQL_CREATE_SCREEN_ITEMS_MENU_BRIDGE = "CREATE TABLE "
                + ScreenItemsModifiersBridgeEntry.TABLE_NAME
                + " ("
                + ScreenItemsModifiersBridgeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ScreenItemsModifiersBridgeEntry.COLUMN_MENU_ITEM_ID + " INTEGER NOT NULL, "
                + ScreenItemsModifiersBridgeEntry.COLUMN_SCREEN_ITEM_ID + " INTEGER NOT NULL"
                + " );";


        db.execSQL(SQL_CREATE_CATEGORY_TABLE);
        db.execSQL(SQL_CREATE_SUB_CATEGORY_TABLE);
        db.execSQL(SQL_CREATE_MENU_TABLE);
        db.execSQL(SQL_CREATE_SCREEN_ITEM_TABLE);
        db.execSQL(SQL_CREATE_SCREEN_ITEMS_MENU_BRIDGE);

    }


    public boolean insertCategory(ItemCategory category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CategoryEntry.COLUMN_CATEGORY_INDEX, category.getCategoryIndex());
        contentValues.put(CategoryEntry.COLUMN_CATEGORY_NAME, category.getCategoryName());
        contentValues.put(CategoryEntry.COLUMN_CATEGORY_NAME_AR, category.getCategoryNameAR());
        contentValues.put(CategoryEntry.COLUMN_IS_ACTIVE, category.getIsActive());
        contentValues.put(CategoryEntry.COLUMN_IS_SCREEN_ONE_VISIBLE, category.getIsScreenOneVisible());
        db.insert(CategoryEntry.TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertSubCategory(ItemSubCategory subCategory, int categoryID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(SubCategoryEntry.COLUMN_IS_ACTIVE, subCategory.getIsActive());
        contentValues.put(SubCategoryEntry.COLUMN_IS_SCREEN_ONE_VISIBLE, subCategory.getIsScreenOneVisible());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_CUSINIE, subCategory.getMenuItemTypeCuisine());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_DESC, subCategory.getMenuItemTypeDesc());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_DESC_AR, subCategory.getMenuItemTypeDescAR());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME, subCategory.getMenuItemTypeName());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_NAME_AR, subCategory.getMenuItemTypeNameAR());
        contentValues.put(SubCategoryEntry.COLUMN_MENU_ITEM_TYPE_IMAGE, subCategory.getMenuItemTypeImage());
        contentValues.put(SubCategoryEntry.COLUMN_TYPE_INDEX, subCategory.getTypeIndex());
        contentValues.put(SubCategoryEntry.COLUMN_CAT_KEY, categoryID);

        db.insert(SubCategoryEntry.TABLE_NAME, null, contentValues);

        return true;
    }

    public boolean insertScreenItem(ItemScreen screen) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues;

        for (Integer i : screen.getScreenItmes()) {
            contentValues = new ContentValues();
            contentValues.put(ScreenItemEntry.COLUMN_MAX_CHOICES, screen.getMaxChoices());
            contentValues.put(ScreenItemEntry.COLUMN_MAX_FREE, screen.getMaxFree());
            contentValues.put(ScreenItemEntry.COLUMN_MIN_CHOICES, screen.getMinChoices());
            contentValues.put(ScreenItemEntry.COLUMN_PARENT_ITEM, screen.getParentItem());
            contentValues.put(ScreenItemEntry.COLUMN_SCREEN_INDEX, screen.getScreenIndex());
            contentValues.put(ScreenItemEntry.COLUMN_SCREEN_NAME, screen.getScreenName());
            contentValues.put(ScreenItemEntry.COLUMN_SCREEN_NAME_AR, screen.getScreenNameAR());
            contentValues.put(ScreenItemEntry.COLUMN_MENU_KEY, i);
            db.insert(ScreenItemEntry.TABLE_NAME, null, contentValues);
        }

        return true;
    }

    public boolean insertMenuItem(ItemMenu menu, int subcategoryID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(MenuEntry.COLUMN_SUB_CATEGORY_KEY, subcategoryID);
        contentValues.put(MenuEntry.COLUMN_ACTIVE, menu.isAcitve());
        contentValues.put(MenuEntry.COLUMN_ALLOW_SPECIAL_INSTRUCTIONS, menu.isAllowSpecialInstructions());
        contentValues.put(MenuEntry.COLUMN_BRANCH_ID, menu.getBranchID());
        contentValues.put(MenuEntry.COLUMN_ESCAPE_MODE, menu.getEscapeMode());
        contentValues.put(MenuEntry.COLUMN_HAS_ADJECTIVES, menu.isHasAdjectives());
        contentValues.put(MenuEntry.COLUMN_IMAGE, menu.getImage());
        contentValues.put(MenuEntry.COLUMN_IS_SCREEN_ONE_VISIBLE, menu.getIsScreenOneVisible());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_CUISINE, menu.getMenuItemCusine());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION, menu.getMenuItemDescription());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION_AR, menu.getMenuItemDescriptionAR());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_NAME, menu.getMenuItemName());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_NAME_AR, menu.getMenuItemNameAR());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_POINTS, menu.getMenuItemPoints());
        contentValues.put(MenuEntry.COLUMN_MENU_ITEM_PRICE, menu.getMenuItemPrice());
        contentValues.put(MenuEntry.COLUMN_MODIFIER, menu.getModifier());
        contentValues.put(MenuEntry.COLUMN_SERVICE_CHARGE, menu.isServiceCharge());
        contentValues.put(MenuEntry.COLUMN_SERVICE_CHARGE_PERCENTAGE, menu.getServiceChargePercentage());
        contentValues.put(MenuEntry.COLUMN_TAX_PERCENTAGE, menu.getTaxPercentage());
        contentValues.put(MenuEntry.COLUMN_TAXABLE, menu.isTaxable());

        db.insert(MenuEntry.TABLE_NAME, null, contentValues);

        return true;
    }

    public Cursor getSubCategories(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + SubCategoryEntry.TABLE_NAME
                + ","
                + CategoryEntry.TABLE_NAME
                + " where "
                + SubCategoryEntry.COLUMN_CAT_KEY + " = " + id
                , null);
        return res;

    }

    public Cursor getCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CategoryEntry.TABLE_NAME, null);

        return res;
    }

    public Cursor getMenu(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + MenuEntry.TABLE_NAME
                + ","
                + SubCategoryEntry.TABLE_NAME
                + " where "
                + MenuEntry.COLUMN_SUB_CATEGORY_KEY + " = " + id
                , null);
        return res;

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + CategoryEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SubCategoryEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MenuEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ScreenItemEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ScreenItemsModifiersBridgeEntry.TABLE_NAME);
        onCreate(db);
    }
}
