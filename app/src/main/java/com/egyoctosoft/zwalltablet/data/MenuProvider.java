package com.egyoctosoft.zwalltablet.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.egyoctosoft.zwalltablet.data.MenuContract.CategoryEntry;
import com.egyoctosoft.zwalltablet.data.MenuContract.SubCategoryEntry;

import static com.egyoctosoft.zwalltablet.data.MenuContract.*;

/**
 * Created by vezikon on 7/1/15.
 */
public class MenuProvider extends ContentProvider {

    private static final int MENU_ITEMS = 100;
    private static final int CATEGORIES = 101;
    private static final int SUBCATEGORIES = 102;
    private static final int SCREEN_ITEMS = 103;
    private static final int MENU_WITH_SCREEN_ITEMS = 104;
    private static final int CATEGORIES_WITH_SUBCATEGORIES_WITH_MENU = 105;
    private static final int SCREEN_ITEMS_WITH_MODIFIERS = 106;
    private static final int SUBCATEGORIES_WITH_MENU = 107;
    private static final int CATEGORIES_WITH_SUBCATEGORIES = 108;
    private static final int SCREEN_ITEMS_MODIFIERS_BRIDGE = 109;
    private static final int SCREEN_ITEMS_MENU_BRIDGE = 110;
    private static final int ONE_MENU_ITEM = 111;


    private static final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        final String authority = CONTENT_AUTHORITY;

        matcher.addURI(authority, PATH_MENU_ITEMS, MENU_ITEMS);
        matcher.addURI(authority, PATH_MENU_ITEMS + "/#/", ONE_MENU_ITEM);
        matcher.addURI(authority, PATH_MENU_ITEMS + "/*", MENU_WITH_SCREEN_ITEMS);
        matcher.addURI(authority, PATH_CATEGORIES, CATEGORIES);
        matcher.addURI(authority, PATH_SUBCATEGORIES, SUBCATEGORIES);
        matcher.addURI(authority, PATH_SCREEN_ITEMS, SCREEN_ITEMS);
        matcher.addURI(authority, PATH_CATEGORIES + "/*/*", CATEGORIES_WITH_SUBCATEGORIES_WITH_MENU);
        matcher.addURI(authority, PATH_SUBCATEGORIES + "/*", SUBCATEGORIES_WITH_MENU);
        matcher.addURI(authority, PATH_CATEGORIES + "/*", CATEGORIES_WITH_SUBCATEGORIES);
        matcher.addURI(authority, PATH_SCREEN_ITEMS_MODIFIERS_BRIDGE, SCREEN_ITEMS_MODIFIERS_BRIDGE);

    }

    // menu query builder containing Categories, Subcategories and Menu item.
    private static final SQLiteQueryBuilder sCategoryWithSubcategoriesWithMenuItems;
    private static final SQLiteQueryBuilder sCategoryWithSubcategories;
    private static final SQLiteQueryBuilder sSubcategoryWithMenuItems;
    private static final SQLiteQueryBuilder sMenuItemsWithScreenItems;

    static {
        sCategoryWithSubcategoriesWithMenuItems = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //Category INNER JOIN Subcategory ON subcategory.category.id = category.id
        // INNER JOIN menu ON menu.subcategory.id = subcategory.id
        sCategoryWithSubcategoriesWithMenuItems.setTables(
                CategoryEntry.TABLE_NAME + " INNER JOIN " +
                        SubCategoryEntry.TABLE_NAME +
                        " ON " + SubCategoryEntry.TABLE_NAME +
                        "." + SubCategoryEntry.COLUMN_CAT_KEY +
                        " = " + CategoryEntry.TABLE_NAME +
                        "." + CategoryEntry._ID +
                        " INNER JOIN " +
                        MenuEntry.TABLE_NAME +
                        " ON " + MenuEntry.TABLE_NAME + "." +
                        MenuEntry.COLUMN_SUB_CATEGORY_KEY +
                        " = " + SubCategoryEntry.TABLE_NAME +
                        "." + SubCategoryEntry._ID);

        //This is an inner join which looks like
        //Category INNER JOIN Subcategory ON subcategory.category.id = category.id
        sCategoryWithSubcategories = new SQLiteQueryBuilder();
        sCategoryWithSubcategories.setTables(
                CategoryEntry.TABLE_NAME + " INNER JOIN " +
                        SubCategoryEntry.TABLE_NAME +
                        " ON " + SubCategoryEntry.TABLE_NAME +
                        "." + SubCategoryEntry.COLUMN_CAT_KEY +
                        " = " + CategoryEntry.TABLE_NAME +
                        "." + CategoryEntry._ID);

        //This is an inner join which looks like
        //subcategory INNER JOIN menu ON menu.subcategory.id = subcategory.id
        sSubcategoryWithMenuItems = new SQLiteQueryBuilder();
        sSubcategoryWithMenuItems.setTables(
                SubCategoryEntry.TABLE_NAME + " INNER JOIN " +
                        MenuEntry.TABLE_NAME + " ON "
                        + MenuEntry.TABLE_NAME + "." + MenuEntry.COLUMN_SUB_CATEGORY_KEY
                        + " = "
                        + SubCategoryEntry.TABLE_NAME + "." + SubCategoryEntry._ID);

        sMenuItemsWithScreenItems = new SQLiteQueryBuilder();
        sMenuItemsWithScreenItems.setTables(
                MenuEntry.TABLE_NAME + " INNER JOIN " +
                        ScreenItemEntry.TABLE_NAME + " ON "
                        + MenuEntry.TABLE_NAME + "." + MenuEntry._ID
                        + " = "
                        + ScreenItemEntry.TABLE_NAME + "." + ScreenItemEntry.COLUMN_PARENT_ITEM
        );
    }

    //subCategory._id = ?
    public static final String sMenuSelection =
            MenuEntry.TABLE_NAME + "." + MenuEntry.COLUMN_SUB_CATEGORY_KEY + " = ? "
                    + " AND " + MenuEntry.COLUMN_MODIFIER + " = 0 " + " AND " + MenuEntry.COLUMN_IS_SCREEN_ONE_VISIBLE
                    + " = 1 ";
    //menu._id = ?
    public static final String sMenuItemDetailSelection = ScreenItemEntry.TABLE_NAME + "." +
            ScreenItemEntry.COLUMN_PARENT_ITEM + " = ? ";

    public static final String sModifiersSelections = ScreenItemsModifiersBridgeEntry.TABLE_NAME
            + "." + ScreenItemsModifiersBridgeEntry.COLUMN_SCREEN_ITEM_ID + " = ?";

    public static final String sOneMenuItemSelection =
            MenuEntry.TABLE_NAME + "." + MenuEntry._ID + " = ? ";

    private MenuDbHelper menuDbHelper;

    @Override
    public boolean onCreate() {
        menuDbHelper = new MenuDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Cursor cursor = null;
        switch (matcher.match(uri)) {
            case MENU_ITEMS:
                cursor = menuDbHelper.getReadableDatabase().query(MenuEntry.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
            case CATEGORIES:
                cursor = menuDbHelper.getReadableDatabase().query(CategoryEntry.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
            case SUBCATEGORIES:
                cursor = menuDbHelper.getReadableDatabase().query(SubCategoryEntry.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
            case CATEGORIES_WITH_SUBCATEGORIES_WITH_MENU:
                cursor = getCategoryWithSubcategoriesWithMenuItems(projection, sortOrder);
                break;
            case SUBCATEGORIES_WITH_MENU:
                cursor = getSubcategoriesWithMenuItems(projection, selection, selectionArgs, sortOrder);
                break;
            case CATEGORIES_WITH_SUBCATEGORIES:
                cursor = getCategoryWithSubcategories(projection, selection, selectionArgs, sortOrder);
                break;
            case MENU_WITH_SCREEN_ITEMS:
                Log.d("this", "called");
                cursor = getMenuItemsWithScreenItems(projection, selection, selectionArgs, sortOrder);
                break;
            case SCREEN_ITEMS_MODIFIERS_BRIDGE:
                cursor = menuDbHelper.getReadableDatabase().query(ScreenItemsModifiersBridgeEntry.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
            case SCREEN_ITEMS:
                cursor = menuDbHelper.getReadableDatabase().query(ScreenItemEntry.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
            case ONE_MENU_ITEM:
                cursor = menuDbHelper.getReadableDatabase().query(MenuEntry.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {

        int match = matcher.match(uri);

        switch (match) {
            case MENU_ITEMS:
                return MenuEntry.CONTENT_TYPE;
            case CATEGORIES:
                return CategoryEntry.CONTENT_TYPE;
            case SUBCATEGORIES:
                return SubCategoryEntry.CONTENT_TYPE;
            case SCREEN_ITEMS:
                return ScreenItemEntry.CONTENT_TYPE;
            case CATEGORIES_WITH_SUBCATEGORIES_WITH_MENU:
                return CategoryEntry.CONTENT_TYPE;
            case SUBCATEGORIES_WITH_MENU:
                return SubCategoryEntry.CONTENT_TYPE;
            case CATEGORIES_WITH_SUBCATEGORIES:
                return CategoryEntry.CONTENT_TYPE;
            case MENU_WITH_SCREEN_ITEMS:
                return MenuEntry.CONTENT_TYPE;
            case SCREEN_ITEMS_MODIFIERS_BRIDGE:
                return ScreenItemsModifiersBridgeEntry.CONTENT_TYPE;
            case ONE_MENU_ITEM:
                return MenuEntry.CONTENT_ITEM_TYPE;
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = menuDbHelper.getWritableDatabase();
        final int match = matcher.match(uri);
        Uri returnUri;

        switch (match) {
            case MENU_ITEMS: {
                long _id = db.insert(MenuEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = MenuEntry.buildMenuUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CATEGORIES: {
                long _id = db.insert(CategoryEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = CategoryEntry.buildCategoriesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SUBCATEGORIES: {
                long _id = db.insert(SubCategoryEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = SubCategoryEntry.buildSubcategoryUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SCREEN_ITEMS: {
                long _id = db.insert(ScreenItemEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = ScreenItemEntry.buildScreenItemUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);

                break;
            }
            case SCREEN_ITEMS_MODIFIERS_BRIDGE: {
                long _id = db.insert(ScreenItemsModifiersBridgeEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = ScreenItemsModifiersBridgeEntry.buildScreenItemUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);

                break;
            }
            default:
                throw new android.database.SQLException("Failed to insert row into " + uri);

        }

        //notify data change in the content provider
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = menuDbHelper.getWritableDatabase();
        final int match = matcher.match(uri);
        int rowsDeleted;

        // this makes delete all rows return the number of rows deleted
        if (null == selection) selection = "1";
        switch (match) {
            case MENU_ITEMS:
                rowsDeleted = db.delete(
                        MenuEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CATEGORIES:
                rowsDeleted = db.delete(
                        CategoryEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SUBCATEGORIES:
                rowsDeleted = db.delete(
                        SubCategoryEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SCREEN_ITEMS:
                rowsDeleted = db.delete(
                        ScreenItemEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SCREEN_ITEMS_MODIFIERS_BRIDGE:
                rowsDeleted = db.delete(
                        ScreenItemsModifiersBridgeEntry.TABLE_NAME, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = menuDbHelper.getWritableDatabase();
        final int match = matcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case MENU_ITEMS:
                rowsUpdated = db.update(MenuEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case CATEGORIES:
                rowsUpdated = db.update(CategoryEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SUBCATEGORIES:
                rowsUpdated = db.update(SubCategoryEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SCREEN_ITEMS:
                rowsUpdated = db.update(ScreenItemEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SCREEN_ITEMS_MODIFIERS_BRIDGE:
                rowsUpdated = db.update(ScreenItemsModifiersBridgeEntry.TABLE_NAME, values, selection
                        , selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    public Cursor getCategoryWithSubcategoriesWithMenuItems(String[] projection, String sortOrder) {

        String[] selectionArgs = null;
        String selection = null;

        return sCategoryWithSubcategoriesWithMenuItems.query(menuDbHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    public Cursor getSubcategoriesWithMenuItems(String[] projection, String selection, String[] selectionArgs,
                                                String sortOrder) {


        return sSubcategoryWithMenuItems.query(menuDbHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    public Cursor getCategoryWithSubcategories(String[] projection, String selection, String[] selectionArgs,
                                               String sortOrder) {


        return sCategoryWithSubcategories.query(menuDbHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    public Cursor getMenuItemsWithScreenItems(String[] projection, String selection, String[] selectionArgs,
                                              String sortOrder) {


        return sMenuItemsWithScreenItems.query(menuDbHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }


}
