package com.egyoctosoft.zwalltablet.managers;

import android.app.Application;

import com.egyoctosoft.zwalltablet.models.ItemCategory;
import com.egyoctosoft.zwalltablet.models.ItemMenu;
import com.egyoctosoft.zwalltablet.models.ItemSubCategory;
import com.egyoctosoft.zwalltablet.models.OpenProfile;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vezikon on 5/19/15.
 */
public class ZwallApp extends Application {

    private static ZwallApp zwallApp;
    private HashMap<String, ArrayList<ItemMenu>> menuHashMap;
    private ArrayList<ItemCategory> categoriesList;
    private ArrayList<ItemSubCategory> subCategoryList;
    private ArrayList<OpenProfile> customers = new ArrayList<>();

    public static ZwallApp getInstance() {
        return zwallApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        zwallApp = this;
    }

    public void setMenuMap(HashMap<String, ArrayList<ItemMenu>> menuHashMap) {
        this.menuHashMap = menuHashMap;
    }

    public HashMap<String, ArrayList<ItemMenu>> getMenuHashMap() {
        return menuHashMap;
    }

    public void setCategoriesList(ArrayList<ItemCategory> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public ArrayList<ItemCategory> getCategoriesList() {
        return categoriesList;
    }

    public ArrayList<ItemSubCategory> getSubCategoryList() {
        return subCategoryList;
    }

    public void setSubCategoryList(ArrayList<ItemSubCategory> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }

    public ArrayList<OpenProfile> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<OpenProfile> customers) {
        this.customers = customers;
    }
}
