package com.egyoctosoft.zwalltablet.managers;

import org.parceler.Parcel;

/**
 * Created by vezikon on 8/4/15.
 */
@Parcel
public class ItemScreenManager {

    int maxFree;
    int maxChoices;
    int minChoices;
    int quantity = 0;
    int currentItemQty = 0;

    public static final int MAX_FREE_COMPLETED = 0;
    public static final int MAX_CHOICES_COMPLETED = 1;
    public static final int NOT_COMPLETE = 2;
    public static final int NO_QUANTITY = 3;


    public int getMaxFree() {
        return maxFree;
    }

    public void setMaxFree(int maxFree) {
        this.maxFree = maxFree;
    }

    public int getMaxChoices() {
        return maxChoices;
    }

    public void setMaxChoices(int maxChoices) {
        this.maxChoices = maxChoices;
    }

    public int getMinChoices() {
        return minChoices;
    }

    public void setMinChoices(int minChoices) {
        this.minChoices = minChoices;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCurrentItemQty() {
        return currentItemQty;
    }

    public void setCurrentItemQty(int currentItemQty) {
        this.currentItemQty = currentItemQty;
    }

    public int increment() {

        if (quantity == maxChoices) {

            return MAX_CHOICES_COMPLETED;
        } else if (quantity == maxFree) {

            ++quantity;
            return MAX_FREE_COMPLETED;
        } else {
            ++quantity;
            return NOT_COMPLETE;
        }
    }

    public int decrement() {
        if (quantity == 0 || currentItemQty == 0)
            return NO_QUANTITY;

        --quantity;
        if (quantity < maxChoices && quantity < maxFree) {
            return NOT_COMPLETE;
        } else {
            return MAX_FREE_COMPLETED;
        }
    }
}
