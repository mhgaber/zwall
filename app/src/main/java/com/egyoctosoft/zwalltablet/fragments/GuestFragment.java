package com.egyoctosoft.zwalltablet.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import com.rey.material.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.managers.ZwallApp;
import com.egyoctosoft.zwalltablet.models.Checkin;
import com.egyoctosoft.zwalltablet.models.Login;
import com.egyoctosoft.zwalltablet.models.OpenProfile;
import com.egyoctosoft.zwalltablet.models.Register;
import com.egyoctosoft.zwalltablet.models.Response;
import com.egyoctosoft.zwalltablet.models.Wallet;
import com.egyoctosoft.zwalltablet.rest.RestClient;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGuestListener} interface
 * to handle interaction events.
 * Use the {@link GuestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuestFragment extends Fragment implements Validator.ValidationListener {


    @InjectView(R.id.btn_guest_login)
    Button btnGuestLogin;
    @InjectView(R.id.guest_login_form)
    LinearLayout guestLoginForm;
    @InjectView(R.id.guest_img)
    ImageView guestImg;
    @InjectView(R.id.btn_register_option)
    Button btnRegisterOption;
    @InjectView(R.id.btn_guest_login_option)
    Button btnGuestLoginOption;
    @InjectView(R.id.btn_cancel_option)
    Button btnCancelOption;
    @InjectView(R.id.guest_welcome_form)
    LinearLayout guestWelcomeForm;
    @NotEmpty
    @InjectView(R.id.guest_name_et)
    EditText guestNameEt;

    private Validator validator;

    private OnGuestListener mListener;

    private ProgressDialog progressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GuestFragment.
     */
    public static GuestFragment newInstance() {
        GuestFragment fragment = new GuestFragment();

        return fragment;
    }

    public GuestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guest, container, false);
        ButterKnife.inject(this, view);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.msg_loading));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        validator = new Validator(this);
        validator.setValidationListener(this);

        btnGuestLoginOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGuestLoginForm(true);
            }
        });

        btnRegisterOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onRegister();
            }
        });
        btnCancelOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onCancel();

                getActivity().onBackPressed();
            }
        });

        btnGuestLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        guestNameEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    validator.validate();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnGuestListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onValidationSucceeded() {
        startLoggingGuestIn(guestNameEt.getText().toString());
    }

    private void startLoggingGuestIn(String name) {
        String phoneNumber,
                date;

        //getting random birth date
        Calendar c = Calendar.getInstance();

        //generating random phone number
        long timeSeed = System.nanoTime(); // to get the current date time value

        double randSeed = Math.random() * 1000; // random number generation

        long midSeed = (long) (timeSeed * randSeed); // mixing up the time and

        String s = midSeed + "";

        phoneNumber = s.substring(0, 9);

        date = (c.get(Calendar.MONTH)) + "-"
                + (c.get(Calendar.DAY_OF_MONTH)) + "-"
                + c.get(Calendar.YEAR);

        Register register = new Register();
        register.setNickname(name);
        register.setFirstname(name);
        register.setEmail("info@zwall.com");
        register.setDateOfBirth(date);
        register.setLastname(" ");
        register.setMphoneno(phoneNumber);
        register.setPassword("55555");
        register.setCoverPic("http://z-wall.com/userfiles/default.jpg");
        register.setPictureURL("http://z-wall.com/userfiles/default.jpg");
        register.setCurrentBranchId("5e378f55-a08e-4475-9708-d9639cc3842d");
        register.setStatus(0);
        register.setUserTypeId(1);
        register.setGenderIsMale(true);

        progressDialog.show();
        register(register);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnGuestListener {
        void onRegister();

        void onGuestLoginSuccess();

        void onCancel();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showGuestLoginForm(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            guestWelcomeForm.setVisibility(show ? View.GONE : View.VISIBLE);
            guestWelcomeForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    guestWelcomeForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            guestLoginForm.setVisibility(show ? View.VISIBLE : View.GONE);
            guestLoginForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    guestLoginForm.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            guestLoginForm.setVisibility(show ? View.VISIBLE : View.GONE);
            guestWelcomeForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private Login login;
    private OpenProfile openProfile;

    public void register(final Register register) {
        RestClient.get().register(register, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (response.getCreateProfileResult().equalsIgnoreCase("success")) {
                    Login login = new Login();
                    login.setPhoneNumber(register.getMphoneno());
                    login.setEncryptedPassword(register.getPassword());
                    Login(login);
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

            }
        });
    }

    public void Login(Login login) {
        this.login = login;
        RestClient.get().login(login, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (!response.getSignInMResult().equalsIgnoreCase("00000000-0000-0000-0000-000000000000"))
                    openProfile(response.getSignInMResult());
                else
                    progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void openProfile(final String id) {
        OpenProfile openProfile = new OpenProfile();
        openProfile.setTarget(id);

        RestClient.get().openProfile(openProfile, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response1) {
                if (response.getOpenProfileResult() != null) {
                    wallet(id);
                    GuestFragment.this.openProfile = response.getOpenProfileResult();
                } else {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void wallet(String id) {
        Wallet wallet = new Wallet();
        wallet.setUserID(id);
        RestClient.get().wallet(wallet, new Callback<Wallet>() {
            @Override
            public void success(Wallet wallet, retrofit.client.Response response) {

                Checkin checkin = new Checkin();
                checkin.setBranchID("5e378f55-a08e-4475-9708-d9639cc3842d");
                checkin.setVenueID(10);
                checkin.setTableId(13);
                checkin.setUserID(openProfile.getID());
                checkin.setPassword(openProfile.getPassword());

                checkIn(checkin);


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void checkIn(Checkin checkin) {
        RestClient.get().checkIn(checkin, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (response.getCheckInResult().equalsIgnoreCase("done")) {
                    Toast.makeText(getActivity(), "Login completed", Toast.LENGTH_LONG).show();

                    if (mListener != null) {
                        mListener.onGuestLoginSuccess();
                    }

                    ZwallApp zApp = ZwallApp.getInstance();
                    zApp.getCustomers().add(openProfile);
                }
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });

    }
}
