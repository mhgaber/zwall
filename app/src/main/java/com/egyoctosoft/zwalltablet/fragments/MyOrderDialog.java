package com.egyoctosoft.zwalltablet.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.StartActivity;
import com.egyoctosoft.zwalltablet.managers.ZwallApp;
import com.egyoctosoft.zwalltablet.models.CreateOrder;
import com.egyoctosoft.zwalltablet.models.ItemModifier;
import com.egyoctosoft.zwalltablet.models.ItemOrder;
import com.egyoctosoft.zwalltablet.models.OpenProfile;
import com.egyoctosoft.zwalltablet.models.OrderDetail;
import com.egyoctosoft.zwalltablet.models.Response;
import com.egyoctosoft.zwalltablet.rest.RestClient;
import com.rey.material.widget.Button;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;

import static com.egyoctosoft.zwalltablet.StartActivity.*;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyOrderDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyOrderDialog extends DialogFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String CART_MAP = "cart.map";
    @InjectView(R.id.order_summary)
    TextView orderSummary;
    @InjectView(R.id.order_btn)
    Button orderBtn;

    private HashMap<String, ArrayList<ItemOrder>> cartMap;
    private static final String TAG = MyOrderDialog.class.getName();
    ProgressDialog progressDialog;
    int index = 0;
    ArrayList<String> users;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param cartMap cart hash map.
     * @return A new instance of fragment MyOrderDialog.
     */
    public static MyOrderDialog newInstance(HashMap<String, ArrayList<ItemOrder>> cartMap) {
        MyOrderDialog fragment = new MyOrderDialog();
        Bundle args = new Bundle();
        args.putParcelable(CART_MAP, Parcels.wrap(cartMap));
        fragment.setArguments(args);
        return fragment;
    }

    public MyOrderDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cartMap = Parcels.unwrap(getArguments().getParcelable(CART_MAP));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_order_dialog, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (cartMap.isEmpty())
            return;

        StringBuilder stringBuilder = new StringBuilder();

        users = new ArrayList<>();
        users.addAll(cartMap.keySet());

        for (String id : users) {
            stringBuilder.append(id + ":").append("\n");

            for (ItemOrder itemOrder : cartMap.get(id)) {

                stringBuilder.append("  " + itemOrder.getMenuItemName());
                stringBuilder.append("\n");

                for (ItemModifier itemModifier : itemOrder.getSides()) {

                    for (int i = 0; i < itemModifier.getCount(); i++) {
                        stringBuilder.append("    " + itemModifier.getMenuItemName());
                        stringBuilder.append("\n");
                    }
                }
            }
        }

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.msg_loading));
        progressDialog.setCancelable(false);

        orderSummary.setText(stringBuilder.toString());

        orderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order();
            }
        });
    }

    private void order() {
        CreateOrder createOrder = new CreateOrder();
        createOrder.setBranchId("5e378f55-a08e-4475-9708-d9639cc3842d");
        createOrder.setVenueid("10");

        OpenProfile openProfile = null;
        for(OpenProfile op : ZwallApp.getInstance().getCustomers()){
            if(users.get(index).equalsIgnoreCase(op.getID())){
                openProfile = op;
                break;
            }
        }
        createOrder.setUserId(openProfile.getID());
        createOrder.setPassword(openProfile.getPassword());


        RestClient.get().createOrder(createOrder, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                addOrderDetail(response.getCreateOrderResult().getID());

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Log.e(TAG, error.toString());
            }
        });

    }

    private void addOrderDetail(String orderId) {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOrderId(orderId);

        OpenProfile openProfile = null;
        for(OpenProfile op : ZwallApp.getInstance().getCustomers()){
            if(users.get(index).equalsIgnoreCase(op.getID())){
                openProfile = op;
                break;
            }
        }

        orderDetail.setUserId(openProfile.getID());
        orderDetail.setPassword(openProfile.getPassword());
        orderDetail.setBranchID("5e378f55-a08e-4475-9708-d9639cc3842d");

        ArrayList<ItemOrder> itemOrders = cartMap.get(openProfile.getID());
        for (ItemOrder itemOrder : itemOrders) {
            orderDetail.getMenuItems().add(itemOrder.getID());
            orderDetail.getIsfreeModefire().add(false);
            orderDetail.getPayInpoints().add(false);
            orderDetail.getSpecialinstructions().add(" ");

            for (ItemModifier itemModifier : itemOrder.getSides()) {

                for (int i = 0; i < itemModifier.getCount(); i++) {
                    orderDetail.getMenuItems().add(itemModifier.getID());
                    orderDetail.getIsfreeModefire().add(false);
                    orderDetail.getPayInpoints().add(false);
                    orderDetail.getSpecialinstructions().add(" ");
                }
            }
        }

        RestClient.get().addOrderDetail(orderDetail, new Callback<retrofit.client.Response>() {
            @Override
            public void success(retrofit.client.Response response, retrofit.client.Response response2) {
                progressDialog.dismiss();
                index++;
                if (index < ZwallApp.getInstance().getCustomers().size()) {
                    order();
                }else{
                    dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Log.e(TAG, error.toString());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
