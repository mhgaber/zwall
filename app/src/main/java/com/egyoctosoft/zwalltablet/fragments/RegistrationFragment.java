package com.egyoctosoft.zwalltablet.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.rey.material.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.managers.ZwallApp;
import com.egyoctosoft.zwalltablet.models.Checkin;
import com.egyoctosoft.zwalltablet.models.Login;
import com.egyoctosoft.zwalltablet.models.OpenProfile;
import com.egyoctosoft.zwalltablet.models.Register;
import com.egyoctosoft.zwalltablet.models.Response;
import com.egyoctosoft.zwalltablet.models.Wallet;
import com.egyoctosoft.zwalltablet.rest.RestClient;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Digits;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnRegistrationListener} interface
 * to handle interaction events.
 * Use the {@link RegistrationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationFragment extends Fragment implements Validator.ValidationListener {


    @InjectView(R.id.header_img)
    ImageView headerImg;
    @NotEmpty
    @InjectView(R.id.login_register_first_name_et)
    EditText mFirstNameE;
    @NotEmpty
    @InjectView(R.id.login_register_last_name_et)
    EditText mLastNameEt;
    @NotEmpty
    @InjectView(R.id.login_register_nickname_et)
    EditText mUsernameEt;
    @NotEmpty
    @Email
    @InjectView(R.id.login_register_email_et)
    EditText mEmailEt;
    @Password(min = 6, scheme = Password.Scheme.ALPHA)
    @InjectView(R.id.login_register_password_et)
    EditText mPasswordEt;
    @ConfirmPassword
    @InjectView(R.id.login_register_confirm_password_et)
    EditText mConfirmPasswordEt;
    @NotEmpty
    @Digits(integer = 10)
    @InjectView(R.id.login_register_mobile_et)
    EditText mMobileNumberEt;
    @InjectView(R.id.login_register_birthdate_tv)
    Button mBirthdayBtn;
    @InjectView(R.id.login_register_gender_male)
    RadioButton mRadioBtnMale;
    @InjectView(R.id.login_register_gender_female)
    RadioButton mRadioBtnFemale;
    @InjectView(R.id.login_gender_radio_group)
    RadioGroup mRadioGroup;
    @InjectView(R.id.login_start_register_btn)
    Button mRegisterBtn;
    @InjectView(R.id.login_registeration_layout)
    RelativeLayout loginRegisterationLayout;
    @InjectView(R.id.login_start_cancel_btn)
    Button cancelBtn;

    private Validator validator;

    private OnRegistrationListener mListener;
    private boolean isMale;
    private ProgressDialog progressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RegistrationFragment.
     */
    public static RegistrationFragment newInstance() {
        RegistrationFragment fragment = new RegistrationFragment();

        return fragment;
    }

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        ButterKnife.inject(this, view);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.msg_loading));
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        mRadioBtnMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRadioButtonClicked(v);
            }
        });
        mRadioBtnFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRadioButtonClicked(v);
            }
        });

        mBirthdayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                birthdayPicker();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnRegistrationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onValidationSucceeded() {
        Register register = new Register();
        register.setFirstname(mFirstNameE.getText().toString());
        register.setLastname(mLastNameEt.getText().toString());
        register.setNickname(mUsernameEt.getText().toString());
        register.setCurrentBranchId("5e378f55-a08e-4475-9708-d9639cc3842d");
        register.setDateOfBirth(mBirthdayBtn.getText().toString());
        register.setEmail(mEmailEt.getText().toString());
        register.setMphoneno(mMobileNumberEt.getText().toString());
        register.setStatus(0);
        register.setUserTypeId(1);
        register.setPictureURL("http://z-wall.com/userfiles/default.jpg");
        register.setCoverPic("http://z-wall.com/userfiles/default.jpg");
        register.setPassword(mPasswordEt.getText().toString());
        register.setGenderIsMale(isMale);

        progressDialog.show();
        register(register);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.login_register_gender_male:
                if (checked)
                    // Pirates are the best
                    isMale = true;
                break;
            case R.id.login_register_gender_female:
                if (checked)
                    // Ninjas rule
                    isMale = false;
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRegistrationListener {
        // TODO: Update argument type and name
        public void onRegistrationSucess();
    }

    private Login login;
    private OpenProfile openProfile;

    public void register(final Register register) {
        RestClient.get().register(register, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (response.getCreateProfileResult().equalsIgnoreCase("success")) {
                    Login login = new Login();
                    login.setPhoneNumber(register.getMphoneno());
                    login.setEncryptedPassword(register.getPassword());
                    Login(login);
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

            }
        });
    }

    public void Login(Login login) {
        this.login = login;
        RestClient.get().login(login, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (!response.getSignInMResult().equalsIgnoreCase("00000000-0000-0000-0000-000000000000"))
                    openProfile(response.getSignInMResult());
                else
                    progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void openProfile(final String id) {
        OpenProfile openProfile = new OpenProfile();
        openProfile.setTarget(id);

        RestClient.get().openProfile(openProfile, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response1) {
                if (response.getOpenProfileResult() != null) {
                    wallet(id);
                    RegistrationFragment.this.openProfile = response.getOpenProfileResult();
                } else {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void wallet(String id) {
        Wallet wallet = new Wallet();
        wallet.setUserID(id);
        RestClient.get().wallet(wallet, new Callback<Wallet>() {
            @Override
            public void success(Wallet wallet, retrofit.client.Response response) {

                Checkin checkin = new Checkin();
                checkin.setBranchID("5e378f55-a08e-4475-9708-d9639cc3842d");
                checkin.setVenueID(10);
                checkin.setTableId(13);
                checkin.setUserID(openProfile.getID());
                checkin.setPassword(openProfile.getPassword());

                checkIn(checkin);


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void checkIn(Checkin checkin) {
        RestClient.get().checkIn(checkin, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (response.getCheckInResult().equalsIgnoreCase("done")) {
                    Toast.makeText(getActivity(), "Login completed", Toast.LENGTH_LONG).show();

                    if (mListener != null) {
                        mListener.onRegistrationSucess();
                    }

                    ZwallApp zApp = ZwallApp.getInstance();
                    zApp.getCustomers().add(openProfile);
                }
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });

    }

    /**
     * =============================================================================================
     * Birthday Picker
     * =============================================================================================
     */


    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            updateDate(month + "-" + day + "-" + year);
        }
    }

    //updating brithday field
    private void updateDate(String date) {
        mBirthdayBtn.setText(date);
    }

    public void birthdayPicker() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }
}
