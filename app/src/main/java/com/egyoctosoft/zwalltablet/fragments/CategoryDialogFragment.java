package com.egyoctosoft.zwalltablet.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.data.MenuContract;
import com.egyoctosoft.zwalltablet.views.adapters.CategoryCursorAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.egyoctosoft.zwalltablet.data.MenuContract.*;


/**
 * Created by vezikon on 7/14/15.
 */
public class CategoryDialogFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    @InjectView(R.id.popup_window_grid_view)
    GridView mGridView;

    private CategoryCursorAdapter cAdapter;

    private OnCategoryDialogListener mListener;


    private final static String[] CAT_COLUMNS = {
            CategoryEntry.TABLE_NAME + "." + CategoryEntry._ID,
            CategoryEntry.COLUMN_CATEGORY_NAME,
            CategoryEntry.COLUMN_CATEGORY_INDEX

    };

    public final static int COL_ID = 0;
    public final static int COL_NAME = 1;
    public final static int COL_INDEX = 2;

    public static CategoryDialogFragment newInstance() {
        CategoryDialogFragment f = new CategoryDialogFragment();

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View v = inflater.inflate(R.layout.dialog_fragment_category, container);

        ButterKnife.inject(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cAdapter = new CategoryCursorAdapter(getActivity(), null, 0);
        mGridView.setAdapter(cAdapter);
        mGridView.setOnItemClickListener(this);

        getActivity().getSupportLoaderManager().initLoader(3, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCategoryDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                CategoryEntry.CONTENT_URI,
                CAT_COLUMNS,
                null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cAdapter.swapCursor(data);


    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        // CursorAdapter returns a cursor at the correct position for getItem(), or null
        // if it cannot seek to that position.
        Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
        if (cursor != null) {
            if (mListener != null)
                mListener.onCategoryChanged(cursor.getInt(COL_ID), cursor.getString(COL_NAME));

            dismiss();

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCategoryDialogListener {
        public void onCategoryChanged(int id, String name);

    }
}
