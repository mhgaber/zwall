package com.egyoctosoft.zwalltablet.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import com.rey.material.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.managers.ZwallApp;
import com.egyoctosoft.zwalltablet.models.Checkin;
import com.egyoctosoft.zwalltablet.models.Login;
import com.egyoctosoft.zwalltablet.models.OpenProfile;
import com.egyoctosoft.zwalltablet.models.Response;
import com.egyoctosoft.zwalltablet.models.Wallet;
import com.egyoctosoft.zwalltablet.rest.RestClient;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Digits;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnLoginListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment implements Validator.ValidationListener {


    @InjectView(R.id.header_img)
    ImageView headerImg;
    @InjectView(R.id.mobile_number)
    @NotEmpty
    @Digits(integer = 10)
    EditText mobileNumber;
    @InjectView(R.id.password)
    @Password(min = 1)
    EditText password;
    @InjectView(R.id.login_btn)
    Button loginBtn;
    @InjectView(R.id.cancel_btn)
    Button cancelBtn;

    private Validator validator;
    private OnLoginListener mListener;
    private ProgressDialog progressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();

        return fragment;
    }

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.inject(this, view);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.msg_loading));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        validator = new Validator(this);
        validator.setValidationListener(this);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validator.validate();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    validator.validate();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onValidationSucceeded() {
        Login login = new Login();
        login.setPhoneNumber(mobileNumber.getText().toString());
        login.setPassword(password.getText().toString());

        progressDialog.show();
        Login(login);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginListener {
        void onLoginSuccess();
    }


    private Login login;
    private OpenProfile openProfile;


    public void Login(Login login) {
        this.login = login;
        RestClient.get().login(login, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (!response.getSignInMResult().equalsIgnoreCase("00000000-0000-0000-0000-000000000000"))
                    openProfile(response.getSignInMResult());
                else
                    progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void openProfile(final String id) {
        OpenProfile openProfile = new OpenProfile();
        openProfile.setTarget(id);

        RestClient.get().openProfile(openProfile, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response1) {
                if (response.getOpenProfileResult() != null) {
                    wallet(id);
                    LoginFragment.this.openProfile = response.getOpenProfileResult();
                } else {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void wallet(String id) {
        Wallet wallet = new Wallet();
        wallet.setUserID(id);
        RestClient.get().wallet(wallet, new Callback<Wallet>() {
            @Override
            public void success(Wallet wallet, retrofit.client.Response response) {

                Checkin checkin = new Checkin();
                checkin.setBranchID("5e378f55-a08e-4475-9708-d9639cc3842d");
                checkin.setVenueID(10);
                checkin.setTableId(13);
                checkin.setUserID(openProfile.getID());
                checkin.setPassword(openProfile.getPassword());

                checkIn(checkin);


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });
    }

    public void checkIn(Checkin checkin) {
        RestClient.get().checkIn(checkin, new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                if (response.getCheckInResult().equalsIgnoreCase("done")) {
                    Toast.makeText(getActivity(), "Login completed", Toast.LENGTH_LONG).show();

                    if (mListener != null) {
                        mListener.onLoginSuccess();
                    }

                    ZwallApp zApp = ZwallApp.getInstance();
                    zApp.getCustomers().add(openProfile);
                }
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.toString());
                progressDialog.dismiss();

            }
        });

    }

}
