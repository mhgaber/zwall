package com.egyoctosoft.zwalltablet.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.egyoctosoft.zwalltablet.MainActivity;
import com.egyoctosoft.zwalltablet.R;
import com.egyoctosoft.zwalltablet.data.MenuProvider;
import com.egyoctosoft.zwalltablet.managers.ItemScreenManager;
import com.egyoctosoft.zwalltablet.models.ItemMenu;
import com.egyoctosoft.zwalltablet.models.ItemModifier;
import com.egyoctosoft.zwalltablet.models.ItemOrder;
import com.egyoctosoft.zwalltablet.models.ItemScreen;
import com.egyoctosoft.zwalltablet.views.adapters.ModifiersAdapter;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

import static com.egyoctosoft.zwalltablet.data.MenuContract.MenuEntry;
import static com.egyoctosoft.zwalltablet.data.MenuContract.ScreenItemEntry;
import static com.egyoctosoft.zwalltablet.data.MenuContract.ScreenItemsModifiersBridgeEntry;
import static com.egyoctosoft.zwalltablet.fragments.MainMenuFragment.OnMenuListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnMenuListener} interface
 * to handle interaction events.
 * Use the {@link ItemDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemDetailsFragment extends DialogFragment
        implements AdapterView.OnItemClickListener, ModifiersAdapter.OnModifiersAdapterClickListener {

    @Optional
    @InjectView(R.id.image)
    ImageView image;
    @Optional
    @InjectView(R.id.item_title)
    TextView itemTitle;
    @Optional
    @InjectView(R.id.item_desc)
    TextView itemDesc;
    @Optional
    @InjectView(R.id.item_points)
    TextView itemPoints;
    @Optional
    @InjectView(R.id.item_price)
    TextView itemPrice;
    @Optional
    @InjectView(R.id.btn_close)
    ImageButton btnClose;
    @Optional
    @InjectView(R.id.modifiers_container)
    LinearLayout modifiersContainer;
    @InjectView(R.id.order)
    com.rey.material.widget.Button order;

    private OnMenuListener mListener;

    private static final String TAG = ItemDetailsFragment.class.getName();

    ArrayList<ItemScreen> itemScreensList = new ArrayList<>();
    ArrayList<ItemModifier> modifiersList = new ArrayList<>();
    //    private OnFragmentInteractionListener mListener;
    ModifiersAdapter adapter;

    //that's for all projections
    public final static int COL_ID = 0;

    private final static String[] MENU_COLUMNS = {
            MenuEntry.TABLE_NAME + "." + MenuEntry._ID,
            MenuEntry.COLUMN_MENU_ITEM_NAME,
            MenuEntry.COLUMN_IMAGE,
            MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION,
            MenuEntry.COLUMN_MENU_ITEM_PRICE,
            MenuEntry.COLUMN_MENU_ITEM_POINTS,
            MenuEntry.COLUMN_HAS_ADJECTIVES,
            MenuEntry.COLUMN_NO_SCREEN_ITEMS
    };

    public final static int COL_NAME = 1;
    public final static int COL_IMG = 2;
    public final static int COL_DESC = 3;
    public final static int COL_PRICE = 4;
    public final static int COL_POINTS = 5;
    public final static int COL_HAS_ADJ = 6;
    public final static int COL_HAS_SCREEN_ITEMS = 7;

    private final static String[] SCREEN_ITEMS_COLUMNS = {
            ScreenItemEntry.TABLE_NAME + "." + ScreenItemEntry._ID,
            ScreenItemEntry.COLUMN_MAX_CHOICES,
            ScreenItemEntry.COLUMN_MAX_FREE,
            ScreenItemEntry.COLUMN_MIN_CHOICES,
            ScreenItemEntry.COLUMN_SCREEN_INDEX,
            ScreenItemEntry.COLUMN_SCREEN_NAME,
            ScreenItemEntry.COLUMN_PARENT_ITEM,
    };

    public final static int COL_MAX_CHOICES = 1;
    public final static int COL_MAX_FREE = 2;
    public final static int COL_MIN_CHOICES = 3;
    public final static int COL_SCREEN_INDEX = 4;
    public final static int COL_SCREEN_NAME = 5;
    public final static int COL_PARENT_ITEM = 6;

    private final static String[] SCREEN_ITEMS_BRIDGE = {
            ScreenItemsModifiersBridgeEntry.COLUMN_MENU_ITEM_ID,
            ScreenItemsModifiersBridgeEntry.COLUMN_SCREEN_ITEM_ID
    };

    private final static int COL_MODIFIER_ID = 0;
    private final static int COL_SCREEN_ITEM_ID = 1;

    @Optional
    @InjectView(R.id.modifier_name)
    TextView modifierName;
    @Optional
    @InjectView(R.id.modifier_price)
    TextView modifierPrice;
    @Optional
    @InjectView(R.id.modifier_quantity)
    TextView modifierQuantity;


    private ItemMenu menuItem;
    private ItemScreenManager manager;

    private SparseArray<ArrayList<ItemModifier>> modifiersSparseArray = new SparseArray<>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param menuItem menu item instance
     * @return A new instance of fragment ItemDetailsFragment.
     */
    public static ItemDetailsFragment newInstance(ItemMenu menuItem) {
        ItemDetailsFragment fragment = new ItemDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(MainActivity.MENU_ITEM, Parcels.wrap(menuItem));
        fragment.setArguments(args);
        return fragment;
    }

    public ItemDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            //getting the menu item sent from the menu to here
            menuItem = Parcels.unwrap(getArguments().getParcelable(MainActivity.MENU_ITEM));

            //start looking for screen items for this menu item
            if (hasScreenItems(menuItem.getID()))
                getScreenItemsMenuItems(itemScreensList.get(0).getID());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_fragment_item_details, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //adding menu item data to the view
        itemTitle.setText(menuItem.getMenuItemName());
        itemDesc.setText(menuItem.getMenuItemDescription());
        itemPoints.setText(menuItem.getMenuItemPoints() + " Point");
        itemPrice.setText(menuItem.getMenuItemPrice() + " L.E.");

        //loading the image
        Picasso.with(getActivity())
                .load(menuItem.getImage())
                .placeholder(R.drawable.item_placeholder).resize(300, 260)
                .into(image);

        if (!itemScreensList.isEmpty())
            setModifiersContainer();

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "has adj: " + menuItem.isHasAdjectives());
                if (menuItem.isHasAdjectives()) {

                    ItemMenu itemMenu = modifiersSparseArray.get(itemScreensList.get(0).getID()).get(0);
                    if (itemMenu.hasScreenItems()) {
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                        DialogFragment dialogFragment = ItemDetailsFragment.newInstance(itemMenu);
                        dialogFragment.setCancelable(false);
                        dialogFragment.show(ft, "itemDetailsDialog");
                    } else {
                        if (mListener != null) {
                            ItemOrder itemOrder = new ItemOrder();

                            itemOrder.setMenuItemName(itemMenu.getMenuItemName());
                            itemOrder.setID(itemMenu.getID());

                            mListener.onAddItem(itemOrder);
                        }
                    }
                } else {
                    if (mListener != null)
                        mListener.onAddItem(getItemOrder());
                }
                dismiss();
            }


        });
    }

    /**
     * @return a new {@link ItemOrder} instance containing {@link ItemModifier} list
     */

    private ItemOrder getItemOrder() {
        ItemOrder itemOrder = new ItemOrder();

        itemOrder.setMenuItemName(menuItem.getMenuItemName());
        itemOrder.setID(menuItem.getID());

        ArrayList<ItemModifier> itemModifierArrayList = new ArrayList<>();

        for (ItemScreen itemScreen : itemScreensList) {
            itemModifierArrayList.addAll(modifiersSparseArray.get(itemScreen.getID()));
        }

        itemOrder.setSides(itemModifierArrayList);

        return itemOrder;
    }


    /**
     * Use this method to start the modifiers container
     * this method will help you check every screen items properties
     * and choose the suitable view for it
     */
    private void setModifiersContainer() {
        TextView title = new TextView(getActivity());
        title.setTextSize(getResources().getDimension(R.dimen.font_large));
        title.setTextColor(getResources().getColor(R.color.gray_dark));
        title.setText(itemScreensList.get(0).getScreenName());
        title.setGravity(Gravity.LEFT);
        title.setTypeface(null, Typeface.BOLD);
        title.setPadding(60, 10, 10, 10);

        //horizontal scrollview params
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        params.setMargins(0, 5, 0, 0);
        title.setLayoutParams(params);

        for (ItemScreen itemScreen : itemScreensList) {
            switch (itemScreen.getMaxChoices()) {
                case 1:
                    modifiersContainer.addView(title);
                    modifiersContainer.addView(
                            addRadioGroupModifierView(modifiersSparseArray.get(itemScreen.getID()),
                                    itemScreen.getID()));
                    break;
                default:
                    title.setBackgroundColor(getResources().getColor(R.color.gray_light));
                    modifiersContainer.addView(title);
                    modifiersContainer.addView(
                            addModifiersView(modifiersSparseArray.get(itemScreen.getID())
                                    , itemScreen.getID()));
            }
        }

    }

    /**
     * Use this method when the Max choices is higher than one
     * this will allow the user to choose more than one item
     *
     * @param modifiersList {@link ItemModifier} list
     * @return list view contains the menu item list added to the modifiers container
     */
    private View addModifiersView(ArrayList<ItemModifier> modifiersList, int screenItemId) {

        ListView listView = new ListView(getActivity());
        listView.setDividerHeight(0);

        //getting screen width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        //horizontal scrollview params
        ListView.LayoutParams params = new ListView.LayoutParams(
                (int) (width * 0.6),
                ListView.LayoutParams.WRAP_CONTENT
        );


        listView.setLayoutParams(params);
        listView.setBackgroundColor(getResources().getColor(R.color.gray_light));
        listView.setPadding(10, 10, 10, 10);


        adapter = new ModifiersAdapter(modifiersList, getActivity(), this, screenItemId);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        return listView;
    }

    /**
     * Use this method when the Max choices equals one
     * this will allow the user to only choose one item
     *
     * @param modifiersList menu items list
     * @return Radio group view contains the menu on item list added to  the modifiers container
     */
    private View addRadioGroupModifierView(final ArrayList<ItemModifier> modifiersList, final int screenItemId) {
        //sometimes there are a lot of choices which require more space
        //so we are gonna use scroll view to put the radio group in it
        HorizontalScrollView scrollView = new HorizontalScrollView(getActivity());

        //getting screen width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        //horizontal scrollview params
        HorizontalScrollView.LayoutParams scrollParams = new HorizontalScrollView.LayoutParams(
                (int) (width * 0.6),
                HorizontalScrollView.LayoutParams.WRAP_CONTENT
        );

        scrollView.setLayoutParams(scrollParams);

        RadioGroup radioGroup = new RadioGroup(getActivity());
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.setGravity(Gravity.CENTER);
        radioGroup.setPadding(30, 30, 30, 30);

        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.MATCH_PARENT,
                RadioGroup.LayoutParams.WRAP_CONTENT
        );

        radioGroup.setLayoutParams(params);

        scrollView.addView(radioGroup);


        RadioButton radioButton;
        for (ItemMenu itemMenu : modifiersList) {
            radioButton = new RadioButton(getActivity());
            radioButton.setId(itemMenu.getID());

            //adding radio button name
            radioButton.setText(itemMenu.getMenuItemName());

            //adding some style

            RadioGroup.LayoutParams _params = new RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT
            );

            _params.setMargins(40, 0, 40, 0);

            radioButton.setLayoutParams(_params);

            radioGroup.addView(radioButton);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(TAG, "radio button id: " + checkedId);
                ArrayList<ItemModifier> modifiers = new ArrayList<ItemModifier>();

                for (ItemModifier itemModifier : modifiersList) {
                    if (checkedId == itemModifier.getID()) {
                        itemModifier.setCount(1);
                        modifiers.add(itemModifier);
                        break;
                    }
                }
                modifiersSparseArray.put(screenItemId, modifiers);
            }
        });


        return scrollView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMenuListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * this method calls the content providers to check if the menu item has screen items or not
     *
     * @param id {@link ItemMenu} id
     * @return true if there are screen items
     */
    private boolean hasScreenItems(int id) {

        String[] selectionArgs = {String.valueOf(id)};

        //getting screen items for this menu item id
        Cursor cursor = getActivity().getContentResolver().query(ScreenItemEntry.CONTENT_URI
                , SCREEN_ITEMS_COLUMNS,
                MenuProvider.sMenuItemDetailSelection,
                selectionArgs,
                null);


        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                ItemScreen itemScreen = new ItemScreen();
                itemScreen.setID(cursor.getInt(COL_ID));
                itemScreen.setMaxChoices(cursor.getInt(COL_MAX_CHOICES));
                itemScreen.setMinChoices(cursor.getInt(COL_MIN_CHOICES));
                itemScreen.setMaxFree(cursor.getInt(COL_MAX_FREE));
                itemScreen.setScreenIndex(cursor.getInt(COL_SCREEN_INDEX));
                itemScreen.setScreenName(cursor.getString(COL_SCREEN_NAME));

                manager = new ItemScreenManager();
                manager.setMaxFree(itemScreen.getMaxFree());
                manager.setMinChoices(itemScreen.getMinChoices());
                manager.setMaxChoices(itemScreen.getMaxChoices());
                manager.setQuantity(0);

                itemScreensList.add(itemScreen);

            } while (cursor.moveToNext());

            return true;
        } else {
            Log.e(TAG, "no item screens");
            return false;
        }
    }

    /**
     * this method gets the modifiers for the current screen item
     *
     * @param id {@link ItemScreen} id
     */
    private void getScreenItemsMenuItems(int id) {

        ArrayList<ItemModifier> itemModifiers;

        String[] selectionArgs = {String.valueOf(id)};

        Cursor cursor = getActivity().getContentResolver().query(ScreenItemsModifiersBridgeEntry.CONTENT_URI
                , SCREEN_ITEMS_BRIDGE,
                MenuProvider.sModifiersSelections,
                selectionArgs,
                null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            itemModifiers = new ArrayList<>();
            do {
                Log.e("" + cursor.getInt(COL_SCREEN_ITEM_ID), "" + cursor.getInt(COL_MODIFIER_ID));
                itemModifiers.add(getModifier(cursor.getInt(COL_MODIFIER_ID)));

            } while (cursor.moveToNext());

            modifiersSparseArray.put(id, itemModifiers);
        } else {
            Log.e(TAG, "no modifiers");
        }
    }


    /**
     * Use this method to get {@link ItemMenu} from the database
     *
     * @param id {@link ItemMenu} id
     * @return a new instance of {@link ItemModifier}
     */
    private ItemModifier getModifier(int id) {
        String[] selectionArgs = {String.valueOf(id)};

        Cursor cursor = getActivity().getContentResolver().query(MenuEntry.buildOneMenuItemUri(id)
                , MENU_COLUMNS,
                MenuProvider.sOneMenuItemSelection,
                selectionArgs,
                null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            ItemModifier itemModifier = new ItemModifier();
            itemModifier.setID(cursor.getInt(COL_ID));
            itemModifier.setMenuItemName(cursor.getString(COL_NAME));
            itemModifier.setImage(cursor.getString(COL_IMG));
            itemModifier.setMenuItemPrice(cursor.getFloat(COL_PRICE));
            itemModifier.setMenuItemDescription(cursor.getString(COL_DESC));
            itemModifier.setHasAdjectives(cursor.getInt(COL_HAS_ADJ) == 0);
            itemModifier.setHasScreenItems(cursor.getInt(COL_HAS_SCREEN_ITEMS) == 1);

            Log.i(TAG, itemModifier.getMenuItemName());

            return itemModifier;
        }

        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Button add = (Button) view.findViewById(R.id.modifier_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "add clicked");
            }
        });
    }

    @Override
    public void onAddClicked(ItemModifier modifier, int position, int screenItemId) {
        Log.i(TAG, "add clicked");

        ArrayList<ItemModifier> modifierArrayList = modifiersSparseArray.get(screenItemId, null);

        if (modifierArrayList == null)
            return;


        switch (manager.increment()) {
            case ItemScreenManager.MAX_FREE_COMPLETED:
                modifierArrayList.get(position).setCount(modifier.getCount() + 1);
                adapter.notifyDataSetChanged();
                break;
            case ItemScreenManager.MAX_CHOICES_COMPLETED:

                break;
            case ItemScreenManager.NOT_COMPLETE:
                modifierArrayList.get(position).setCount(modifier.getCount() + 1);
                adapter.notifyDataSetChanged();
                break;
        }


    }

    @Override
    public void onSubtractClicked(ItemModifier modifier, int position, int screenItemId) {
        Log.i(TAG, "sub clicked");

        manager.setCurrentItemQty(modifier.getCount());

        ArrayList<ItemModifier> modifierArrayList = modifiersSparseArray.get(screenItemId, null);

        if (modifierArrayList == null)
            return;

        switch (manager.decrement()) {
            case ItemScreenManager.MAX_FREE_COMPLETED:
                modifierArrayList.get(position).setCount(modifier.getCount() - 1);
                adapter.notifyDataSetChanged();
                break;
            case ItemScreenManager.NOT_COMPLETE:
                modifierArrayList.get(position).setCount(modifier.getCount() - 1);
                adapter.notifyDataSetChanged();
                break;

        }


    }
}
