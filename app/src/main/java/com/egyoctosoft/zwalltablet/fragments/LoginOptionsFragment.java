package com.egyoctosoft.zwalltablet.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.rey.material.widget.TextView;

import com.egyoctosoft.zwalltablet.R;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * A placeholder fragment containing a simple view.
 */
public class LoginOptionsFragment extends Fragment implements View.OnClickListener {

    @InjectView(R.id.header_img)
    ImageView headerImg;
    @InjectView(R.id.btn_login)
    TextView btnLogin;
    @InjectView(R.id.btn_new_user)
    TextView btnNewUser;
    @InjectView(R.id.btn_guest)
    TextView btnGuest;

    private OnLoginOptionListener mListener;


    public static final int OPTION_LOGIN = 0;
    public static final int OPTION_REGISTER = 1;
    public static final int OPTION_GUEST = 2;

    public LoginOptionsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_options, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnLoginOptionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnLogin.setOnClickListener(this);
        btnGuest.setOnClickListener(this);
        btnNewUser.setOnClickListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_login:
                if (mListener != null)
                    mListener.onOptionClicked(OPTION_LOGIN);
                break;
            case R.id.btn_guest:
                if (mListener != null)
                    mListener.onOptionClicked(OPTION_GUEST);
                break;
            case R.id.btn_new_user:
                if (mListener != null)
                    mListener.onOptionClicked(OPTION_REGISTER);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginOptionListener {
        void onOptionClicked(int option);
    }
}
