package com.egyoctosoft.zwalltablet.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.egyoctosoft.zwalltablet.R;

import com.egyoctosoft.zwalltablet.data.MenuProvider;
import com.egyoctosoft.zwalltablet.models.ItemMenu;
import com.egyoctosoft.zwalltablet.models.ItemOrder;
import com.egyoctosoft.zwalltablet.views.adapters.MainMenuRecyclerViewAdapter;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

import static com.egyoctosoft.zwalltablet.data.MenuContract.*;
import static com.egyoctosoft.zwalltablet.views.adapters.MainMenuRecyclerViewAdapter.*;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnMenuListener}
 * interface.
 */
public class MainMenuFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>, OnMenuItemClickListener {


    private OnMenuListener mListener;

    private final static String[] MENU_COLUMNS = {
            MenuEntry.TABLE_NAME + "." + MenuEntry._ID,
            MenuEntry.COLUMN_MENU_ITEM_NAME,
            MenuEntry.COLUMN_IMAGE,
            MenuEntry.COLUMN_MENU_ITEM_DESCRIPTION,
            MenuEntry.COLUMN_MENU_ITEM_PRICE,
            MenuEntry.COLUMN_NO_SCREEN_ITEMS,
            MenuEntry.COLUMN_HAS_ADJECTIVES
    };

    public final static int COL_ID = 0;
    public final static int COL_NAME = 1;
    public final static int COL_IMG = 2;
    public final static int COL_DESC = 3;
    public final static int COL_PRICE = 4;
    public final static int COL_NO_SCREEN_ITEMS = 5;
    public final static int COL_HAS_ADJ = 6;

    private static final String KEY_SUBCATEGORY_ID = "sub.id";

    private int subCat_Id;

    /**
     * The fragment's ListView/GridView.
     */
    private RecyclerView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private MainMenuRecyclerViewAdapter mAdapter;
    private RecyclerViewMaterialAdapter recyclerViewMaterialAdapter;


    private ArrayList<ItemMenu> menu = new ArrayList<>();

    // TODO: Rename and change types of parameters
    public static MainMenuFragment newInstance(ArrayList<ItemMenu> menu) {
        MainMenuFragment fragment = new MainMenuFragment();
        Bundle args = new Bundle();
        args.putParcelable("", Parcels.wrap(menu));
        fragment.setArguments(args);
        return fragment;
    }

    public static MainMenuFragment newInstance(int subCategoryId) {
        MainMenuFragment fragment = new MainMenuFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_SUBCATEGORY_ID, subCategoryId);
        fragment.setArguments(args);
        return fragment;
    }


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MainMenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            menu = Parcels.unwrap(getArguments().getParcelable(""));
//
//        }

        if (getArguments() != null) {
            subCat_Id = getArguments().getInt(KEY_SUBCATEGORY_ID);
        }

        mAdapter = new MainMenuRecyclerViewAdapter(menu, getActivity(), this);

//        // TODO: Change Adapter to display your content
//        mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_itemmenu, container, false);

        // Set the adapter
        mListView = (RecyclerView) view.findViewById(android.R.id.list);

        //getting to know screen size so it can decide number of columns according
        // to screen size and state
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {

            if (isLandScape()) {
                mListView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                recyclerViewMaterialAdapter = new RecyclerViewMaterialAdapter(mAdapter, 3);
            } else {
                mListView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                recyclerViewMaterialAdapter = new RecyclerViewMaterialAdapter(mAdapter, 2);
            }

        } else {
            if (!isLandScape()) {

                // do something else
                mListView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                recyclerViewMaterialAdapter = new RecyclerViewMaterialAdapter(mAdapter, 1);
            } else {
                mListView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                recyclerViewMaterialAdapter = new RecyclerViewMaterialAdapter(mAdapter, 2);
            }

        }

        mListView.setHasFixedSize(true);
        mListView.setAdapter(recyclerViewMaterialAdapter);

        //registering the recycler view to the martial view pager
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mListView, null);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //start the loader
        getActivity().getSupportLoaderManager().initLoader(subCat_Id, null, this);

    }


    private boolean isLandScape() {
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();

        int orientation = display.getRotation();

        if (orientation == Surface.ROTATION_90
                || orientation == Surface.ROTATION_270) {
            return false;
        }

        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMenuListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
//        View emptyView = mListView.getEmptyView();
//
//        if (emptyView instanceof TextView) {
//            ((TextView) emptyView).setText(emptyText);
//        }
    }

    /**
     * use this method to show item details dialog
     *
     * @param itemMenu the menu item
     */
    private void showDetails(ItemMenu itemMenu) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        DialogFragment dialogFragment = ItemDetailsFragment.newInstance(itemMenu);
        dialogFragment.setCancelable(false);
        dialogFragment.show(ft, "itemDetailsDialog");
    }

    /*
     * loader methods
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] selectionArgs = {String.valueOf(subCat_Id)};

        return new CursorLoader(getActivity(),
                MenuEntry.CONTENT_URI
                , MENU_COLUMNS,
                MenuProvider.sMenuSelection,
                selectionArgs,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if (data.getCount() > 0) {
            data.moveToFirst();

            do {
                ItemMenu itemMenu = new ItemMenu();
                itemMenu.setID(data.getInt(COL_ID));
                itemMenu.setMenuItemName(data.getString(COL_NAME));
                itemMenu.setImage(data.getString(COL_IMG));
                itemMenu.setMenuItemPrice(data.getFloat(COL_PRICE));
                itemMenu.setMenuItemDescription(data.getString(COL_DESC));
                Log.d("no screen items", data.getString(COL_NO_SCREEN_ITEMS));
                Log.d("boolean screen items", "" + Boolean.parseBoolean(data.getString(COL_NO_SCREEN_ITEMS)));

                itemMenu.setHasScreenItems(data.getInt(COL_NO_SCREEN_ITEMS) == 1);
                itemMenu.setHasAdjectives(data.getInt(COL_HAS_ADJ) == 0);
//
//                //Screen item
//                ItemScreen itemScreen = new ItemScreen();
//                itemScreen.setScreenName(data.getString(COL_SCREEN_ITEM_NAME));
//                Log.e("screen item name", itemScreen.getScreenName());
//
//                itemMenu.getItemScreens().add(itemScreen);

                menu.add(itemMenu);
            } while (data.moveToNext());

            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    /*
     * Menu item listeners
     */
    @Override
    public void onMenuItemOrder(ItemMenu itemMenu) {
        if (mListener != null) {
            ItemOrder itemOrder = new ItemOrder();

            itemOrder.setMenuItemName(itemMenu.getMenuItemName());
            itemOrder.setID(itemMenu.getID());

            mListener.onAddItem(itemOrder);
        }

    }

    @Override
    public void onItemDetails(ItemMenu itemMenu) {
        showDetails(itemMenu);
    }

    @Override
    public void onMenuItemHasModifiers(ItemMenu itemMenu) {
        showDetails(itemMenu);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMenuListener {
        void onAddItem(ItemOrder itemOrder);
    }


}
